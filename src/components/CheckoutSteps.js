import React from 'react'

export default function CheckoutSteps(props) {
    return (
        <div className="d-md-flex justify-content-center my-5">
            <div className={props.step1 ? 'bg-info text-white p-3 border-left border-right rounded-lg mb-2': 'mb-2 rounded-lg border-left border-right p-3'}>Se connecter</div>
            <div className={props.step2 ? 'bg-info text-white p-3 border-left border-right rounded-lg mb-2': 'mb-2 rounded-lg border-left border-right p-3'}>Expédié</div>
            <div className={props.step3 ? 'bg-info text-white p-3 border-left border-right rounded-lg mb-2': 'mb-2 rounded-lg border-left border-right p-3'}>Paiement</div>
            <div className={props.step4 ? 'bg-info text-white p-3 border-left border-right rounded-lg mb-2': 'mb-2 rounded-lg border-left border-right p-3'}>Commander</div>
        </div>
    )
}