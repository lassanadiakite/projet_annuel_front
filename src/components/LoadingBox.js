import React from 'react'

export default function LoadingBox() {
    return (
        <div className="loading text-center my-5">
            <i className="fa fa-spinner fa-spin" /> Loading...
        </div>
    )
}