import React from 'react'
import { Link } from 'react-router-dom';
import Rating from './Rating';
import {Card} from "react-bootstrap";

export default function Product(props) {
    const { product } = props;
    return (
    	<div className="col-lg-3 col-md-4 mx-auto my-5">
		    <Link to={`/product/${ product.id }`} style={{ textDecoration: 'none', color: '#000' }}>
			    <Card>
				    <Card.Img variant="top" src={ product.image } alt={ product.name } />
				    <Card.Body>
					    <Card.Title className="text-center">{ product.name }</Card.Title>
					    <Card.Text className="text-center">
						    <p>{ product.price } €</p>
						    { product.user_id.firstName ?
							    <p>
								    <a href={`/seller/${product.user_id.id}`} className="text-info text-decoration-none">
									    { product.user_id.companyName }
								    </a>
							    </p>
							    :
							    " "
						    }
					    </Card.Text>
				    </Card.Body>
				    <Card.Footer className="text-center">
					    <Rating rating={ product.rating } numReviews={ product.numReviews } />
				    </Card.Footer>
			    </Card>
		    </Link>
	    </div>
    )
}