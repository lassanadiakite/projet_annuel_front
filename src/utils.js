export const prices = [
    {
      name: 'Aucun',
      min: 0,
      max: 0,
    },
    {
      name: `de 1 € à 10 €`,
      min: 1,
      max: 10,
    },
    {
      name: `10 € à 100€`,
      min: 10,
      max: 100,
    },
    {
      name: `100 € à 1000€`,
      min: 100,
      max: 1000,
    },
  ];
  export const ratings = [
    {
      name: '4 étoiles et plus',
      rating: 4,
    },
  
    {
      name: '3 étoiles et plus',
      rating: 3,
    },
  
    {
      name: '2 étoiles et plus',
      rating: 2,
    },
  
    {
      name: '1 étoiles et plus',
      rating: 1,
    },
  ];