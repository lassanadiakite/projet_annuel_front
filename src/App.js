import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {BrowserRouter, Route} from 'react-router-dom'
import {signout} from './actions/userActions';
import AdminRoute from './components/AdminRoute';
import SellerRoute from './components/SellerRoute';
import PrivateRoute from './components/PrivateRoute';
import CartScreen from './screens/CartScreen';
import HomeScreen from './screens/HomeScreen';
import OrderHistoryScreen from './screens/OrderHistoryScreen';
import OrderListScreen from './screens/OrderListScreen';
import OrderScreen from './screens/OrderScreen';
import PaymentMethodScreen from './screens/PaymentMethodScreen';
import PlaceOrderScreen from './screens/PlaceOrderScreen';
import ProductEditScreen from './screens/ProductEditScreen';
import ProductCreateScreen from './screens/ProductCreateScreen';
import ProductListScreen from './screens/ProductListScreen';
import ProductScreen from './screens/ProductScreen';
import ProfileScreen from './screens/ProfileScreen';
import RegisterScreen from './screens/RegisterScreen';
import ShippingAddressScreen from './screens/ShippingAddressScreen';
import SigninScreen from './screens/SigninScreen';
import UserEditScreen from './screens/UserEditScreen';
import UserListScreen from './screens/UserListScreen';
import SellerScreen from './screens/SellerScreen';
import MapScreen from './screens/MapScreen'
import {Badge, Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag, faUser } from '@fortawesome/free-solid-svg-icons';


function App() {
	
	const cart = useSelector(state => state.cart);
	const {cartItems} = cart;
	const userSignin = useSelector((state) => state.userSignin);
	const {userInfo} = userSignin;
	const dispatch = useDispatch();
	
	const signoutHandler = () => {
		dispatch(signout());
	}
	
	return (
		<BrowserRouter>
			<header>
				<Navbar bg="light" variant="light" expand="lg">
					<Container className="py-1">
						<Navbar.Brand href="/">ProxiCommerce</Navbar.Brand>
						<Navbar.Toggle aria-controls="basic-navbar-nav" />
						<Navbar.Collapse id="basic-navbar-nav">
							<Nav className="ml-auto">
								{
									userInfo ? (
										<NavDropdown title={ userInfo.firstName } id="basic-nav-dropdown">
											<NavDropdown.Item href="/profile">Paramètres</NavDropdown.Item>
											<NavDropdown.Item href="/orderhistory">Mes commandes</NavDropdown.Item>
											<NavDropdown.Item href="/" onClick={signoutHandler}>Se deconnecter</NavDropdown.Item>
										</NavDropdown>
										) :
										(
											<NavDropdown title={ <FontAwesomeIcon icon={faUser} style={{ fontSize: 20 }} /> } id="basic-nav-dropdown">
												<NavDropdown.Item href="/signin">Se connecter</NavDropdown.Item>
												<NavDropdown.Item href="/register">Créer un compte</NavDropdown.Item>
											</NavDropdown>
										)
								}
								{
									userInfo && userInfo.roles.find(element => element === "ROLE_SELLER") &&
									(
										<NavDropdown title="Vendeur" id="basic-nav-dropdown">
											<NavDropdown.Item href="/productlist/seller">Produits commercialisés</NavDropdown.Item>
											<NavDropdown.Item href="/orderlist/seller">Commandes reçu</NavDropdown.Item>
										</NavDropdown>
									)
								}
								{
									userInfo && userInfo.roles.find(element => element === "ROLE_ADMIN") &&
									(
										<NavDropdown title='Admin' id="basic-nav-dropdown">
											<NavDropdown.Item href="/productlist">Produits commercialisés</NavDropdown.Item>
											<NavDropdown.Item href="/orderlist">Total des commandes effectués</NavDropdown.Item>
											<NavDropdown.Item href="/userlist">Gestion des utilisateurs</NavDropdown.Item>
										</NavDropdown>
									)
								}
								<Nav.Link href="/cart">
									<FontAwesomeIcon icon={faShoppingBag} style={{ fontSize: 20 }} />
									{ cartItems.length === 0 ? (<Badge className="bg-dark ml-1 text-white">0</Badge>) :
										(<Badge className="bg-danger ml-1 text-white">{ cartItems.length }</Badge>) }
								</Nav.Link>
							</Nav>
						</Navbar.Collapse>
					</Container>
				</Navbar>
			</header>
			
			<main>
				<Route path="/productlist/create" component={ProductCreateScreen} />
			    <Route path="/seller/:id" component={SellerScreen} />
				<Route path="/cart/:id?" component={CartScreen} />
				<Route path="/product/:id" component={ProductScreen} exact />
				<Route path="/product/:id/edit" component={ProductEditScreen} exact />
				<Route path="/signin" component={SigninScreen} />
				<Route path="/register" component={RegisterScreen} />
				<Route path="/shipping" component={ShippingAddressScreen} exact />
				<Route path="/payment" component={PaymentMethodScreen} exact />
				<Route path="/placeorder" component={PlaceOrderScreen} exact />
				<Route path="/order/:id" component={OrderScreen} />
				<Route path="/orderhistory" component={OrderHistoryScreen} />
				<PrivateRoute path="/profile" component={ProfileScreen} />
				<PrivateRoute path="/map" component={MapScreen} />
				<AdminRoute path="/productlist" component={ProductListScreen} exact />
				<AdminRoute path="/orderlist" component={OrderListScreen} exact />
				<AdminRoute path="/userlist" component={UserListScreen} />
				<AdminRoute path="/user/:id/edit" component={UserEditScreen} />
				<SellerRoute path="/productlist/seller" component={ProductListScreen} />
				<SellerRoute path="/orderlist/seller" component={OrderListScreen} />
				<Route path="/" component={HomeScreen} exact />
			</main>
			
			<footer className="py-1 bg-light">
				<p className="text-center text-dark my-3">ProxiCommercer | Tous droits réservés</p>
			</footer>
		</BrowserRouter>
	);
}

export default App;
