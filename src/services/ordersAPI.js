import axios from "axios";
import { ORDERS_API } from "../config";

function findAll() {
  return axios
    .get(ORDERS_API)
    .then(response => response.data["hydra:member"]);
}

function deleteOrder(id) {
  return axios.delete(ORDERS_API + "/" + id);
}

function find(id) {
  return axios.get(ORDERS_API + "/" + id).then(response => response.data);
}
function mine() {
 // return axios.get(ORDERS_API + "/" + id).then(response => response.data);
  return axios
    .get(ORDERS_API)
    .then(response => response.data["hydra:member"]);
}

function update(id, order) {
  return axios.put(ORDERS_API + "/" + id, {
    ...order,
    isPaid: true,
    paidAt: Date.now(),
    shippingAddress: `/shipping_addresses/${order.shippingAddress}`,
    paymentResult: `/payment_results/${order.paymentResult}`,
  });
}


function create(order) {
  return axios.post(ORDERS_API, {
    ...order,
    customer: `/users/${order.customer}`
  });
}

export default {
  mine,
  findAll,
  find,
  create,
  update,
  delete: deleteOrder
};
