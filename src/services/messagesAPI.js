import axios from "axios";
import { MESSAGES_API } from "../config";

function findAll() {
  return axios
    .get(MESSAGES_API)
    .then(response => response.data["hydra:member"]);
}

function deleteMessage(id) {
  return axios.delete(MESSAGES_API + "/" + id);
}

function find(id) {
  return axios.get(MESSAGES_API + "/" + id).then(response => response.data);
}

function update(id, message) {
  return axios.put(MESSAGES_API + "/" + id, {
    ...message
  });
}

function create(message) {
  return axios.post(MESSAGES_API, {
    ...message
  });
}

export default {
  findAll,
  find,
  create,
  update,
  delete: deleteMessage
};
