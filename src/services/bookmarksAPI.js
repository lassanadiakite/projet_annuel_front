import axios from "axios";
import { BOOKMARKS_API } from "../config";

function findAll() {
  return axios
    .get(BOOKMARKS_API)
    .then(response => response.data["hydra:member"]);
}

function deleteProduct(id) {
  return axios.delete(BOOKMARKS_API + "/" + id);
}

function find(id) {
  return axios.get(BOOKMARKS_API + "/" + id).then(response => response.data);
}

function update(id, product) {
  return axios.put(BOOKMARKS_API + "/" + id, {
    ...product,
    category: `/categories/${product.category}`,
  });
}

function create(product) {
  return axios.post(BOOKMARKS_API, {
    ...product,
    category: `/categories/${product.category}`,
  });
}

export default {
  findAll,
  find,
  create,
  update,
  delete: deleteProduct
};
