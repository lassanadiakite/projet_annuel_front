export const API_URL="https://esgiprojet-annuel-apiplatform.herokuapp.com/"      //"http://my-alb-eae5ace03db50213.elb.us-east-1.amazonaws.com:8000/"  ;// https://localhost:8443/

//export const API_URL = process.env.API_URL;

export const PAYPAL_API = API_URL + "config/paypal";
export const BOOKMARKS_API = API_URL + "bookmarks";
export const CATEGORIES_API = API_URL + "categories";
export const PRODUCTS_API = API_URL + "products";
export const COMMENTS_API = API_URL + "comments";
export const MESSAGES_API = API_URL + "messages";
export const ORDERS_API = API_URL + "orders";
export const USERS_API = API_URL + "users";
export const PAYMENT_RESULTS_API = API_URL + "payment_results";
export const DELIVS_API = API_URL + "deliv/orders";
export const LOGIN_API = API_URL + "authentication_token";//
