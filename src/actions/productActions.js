import {
    PRODUCT_CREATE_REQUEST,
    PRODUCT_CREATE_SUCCESS,
    PRODUCT_CREATE_FAIL,
    PRODUCT_LIST_REQUEST,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_DETAILS_REQUEST,
    PRODUCT_DETAILS_SUCCESS,
    PRODUCT_DETAILS_FAIL,
    PRODUCT_UPDATE_REQUEST,
    PRODUCT_UPDATE_SUCCESS,
    PRODUCT_UPDATE_FAIL,
    PRODUCT_DELETE_REQUEST,
    PRODUCT_DELETE_SUCCESS,
    PRODUCT_DELETE_FAIL,
    PRODUCT_REVIEW_CREATE_REQUEST,
    PRODUCT_REVIEW_CREATE_SUCCESS,
    PRODUCT_REVIEW_CREATE_FAIL,
} from '../constants/productConstants.js';
import ProductsAPI from '../services/productsAPI.js';

import { PRODUCTS_API, COMMENTS_API } from "../config.js";
import axios from "axios";

export const listProducts = ({user_id = ''}) => async (dispatch) =>{
    dispatch({
        type: PRODUCT_LIST_REQUEST,
    });
    try {
        const  data  = await axios
            .get(PRODUCTS_API+ "?user_id.id=" + `${user_id}`)
            .then(response => response.data["hydra:member"]);

       /* const filteredProducts = data.filter(
                            c =>
                              c.name.toLowerCase().includes(search.toLowerCase()) ||
                              c.category.name.toLowerCase().includes(search.toLowerCase()) ||
                              c.price.toLowerCase().includes(search.toLowerCase()) 
                          );
                          console.log(filteredProducts)*/
        dispatch({ type: PRODUCT_LIST_SUCCESS, payload: data });
    }catch(error) {
        dispatch({ type: PRODUCT_LIST_FAIL, payload: error.message });
    }
};

export const detailsProduct = (productId) => async(dispatch) =>{
    dispatch({ type: PRODUCT_DETAILS_REQUEST, payload: productId});
    try{
        const data = await ProductsAPI.find(productId);
        dispatch({ type: PRODUCT_DETAILS_SUCCESS, payload: data})
    } catch(error){
        dispatch({ type: PRODUCT_DETAILS_FAIL,
            payload: 
                error.response && error.response.data.message 
                    ? error.response.data.message
                    : error.message,
        });
    }
};

export const createProduct = (product /*name, price, image, category, brand, countInStock, description*/) => async (dispatch, getState) => {
    dispatch({ type: PRODUCT_CREATE_REQUEST});
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    console.log(product.category)
    try {
        const { data } =  await axios.post(PRODUCTS_API, {
            ...product
        } ,{
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
            }
        );
        dispatch({ type: PRODUCT_CREATE_SUCCESS, payload: data.product });
    } catch(error) {
        const message = 
        error.response && error.response.data.message 
        ? error.response.data.message
        : error.message;
        dispatch({type: PRODUCT_CREATE_FAIL, payload: message })
    }
};

export const updateProduct = (product) => async(dispatch, getState) => {
    dispatch({ type: PRODUCT_UPDATE_REQUEST, payload: product});
    const {userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        //console.log(product)
        const { data } =  await axios.put(PRODUCTS_API+ "/" + `${product.id}`, {
                ...product,
                //user_id: `/users/${product.user_id.id}`,
                category: `/categories/${product.category.id}`
            }/*product*/ ,{
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
            }
        );
        dispatch({ type: PRODUCT_UPDATE_SUCCESS, payload: data });
    } catch(error) {
        const message = 
        error.response && error.response.data.message 
        ? error.response.data.message
        : error.message;
        dispatch({type: PRODUCT_UPDATE_FAIL, error: message })
    }
};

export const deleteProduct = (productId) => async (dispatch, getState) => {
    dispatch({ type: PRODUCT_DELETE_REQUEST, payload: productId});
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        const { data } =  await axios.delete(PRODUCTS_API + "/" + `${productId}`, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });
        dispatch({ type: PRODUCT_DELETE_SUCCESS });
    } catch(error) {
        const message = 
        error.response && error.response.data.message 
        ? error.response.data.message
        : error.message;
        dispatch({type: PRODUCT_DELETE_FAIL, payload: message })
    }
};

export const createReview = (productId, review) => async (
    dispatch,
    getState
  ) => {
    dispatch({ type: PRODUCT_REVIEW_CREATE_REQUEST });
    const {
      userSignin: { userInfo },
    } = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
      const { data } = await axios.post(COMMENTS_API, {
            ...review
        } ,//`/products/${productId}/comments`,
        
        {
            headers: { Authorization: `Bearer ${token1}`,
          //Authorization: `Bearer ${userInfo.token}`,
            },
        }
      );
      dispatch({
        type: PRODUCT_REVIEW_CREATE_SUCCESS,
        payload: data.review,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({ type: PRODUCT_REVIEW_CREATE_FAIL, payload: message });
    }
  };