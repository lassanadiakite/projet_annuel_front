import jwtDecode from "jwt-decode";
import { 
    USER_SIGNIN_FAIL, 
    USER_SIGNIN_REQUEST, 
    USER_SIGNIN_SUCCESS, 
    USER_REGISTER_REQUEST, 
    USER_REGISTER_SUCCESS, 
    USER_REGISTER_FAIL, 
    USER_SIGNOUT, 
    USER_DETAILS_FAIL, 
    USER_DETAILS_REQUEST, 
    USER_DETAILS_SUCCESS, 
    USER_UPDATE_PROFILE_REQUEST, 
    USER_UPDATE_PROFILE_SUCCESS, 
    USER_UPDATE_PROFILE_FAIL, 
    USER_LIST_REQUEST,
    USER_LIST_SUCCESS,
    USER_LIST_FAIL,
    USER_DELETE_REQUEST,
    USER_DELETE_SUCCESS,
    USER_DELETE_FAIL,
    USER_UPDATE_REQUEST,
    USER_UPDATE_SUCCESS,
    USER_UPDATE_FAIL
} from "../constants/userConstants"
import authAPI from "../services/authAPI";
import usersAPI from "../services/usersAPI";
import { USERS_API, LOGIN_API } from "../config.js";
import axios from "axios";
/**
 * Positionne le token JWT sur Axios
 * @param {string} token Le token JWT
 */
 function setAxiosToken(token) {
    axios.defaults.headers["Authorization"] = "Bearer " + token;
  }
export const register = (firstName, lastName, email, password) => async(dispatch) => {
    dispatch({ type: USER_REGISTER_REQUEST, payload: {email, password } });
    var data1 = {}
        try {
        const { data }  = await axios
        .post(USERS_API, {firstName, lastName, email, password})
        
          // Je stocke le token dans mon localStorage
          console.log(data)
          //window.localStorage.setItem("userInfo", JSON.stringify(data));
         // window.localStorage.setItem("userInfo", token);
          // On prévient Axios qu'on a maintenant un header par défaut sur toutes nos futures requetes HTTP
         // setAxiosToken(data.token);
         // const data1 = jwtDecode(data.token);
            data1 = data
         // console.log(data1)
           //signin(email, password)
          dispatch({ type: USER_REGISTER_SUCCESS, payload: data});
        //dispatch({ type: USER_SIGNIN_SUCCESS, payload: (data)});
       // localStorage.setItem("userInfo", JSON.stringify(data1));

        
        
        //console.log(data.email)
        //const data1 = jwtDecode(data);
        //console.log(data1)
       // localStorage.setItem("userInfo", JSON.stringify(data));
    }
    /*try {
        const data = await usersAPI.register(firstName, lastName, email, password);
        dispatch({ type: USER_REGISTER_SUCCESS, payload: data});
        dispatch({ type: USER_SIGNIN_SUCCESS, payload: data});
        //console.log(data)
        //const data1 = jwtDecode(data);
        //console.log(data1)
        localStorage.setItem("userInfo", JSON.stringify(data));
    } */catch(error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: 
                 
            error.response && error.response.data.message 
            ? error.response.data.message
            : error.message,
        });
    } finally {
        const data = await axios
          .post(LOGIN_API, {email, password})
          .then(response => response.data.token)
          .then(token => {
            // Je stocke le token dans mon localStorage
            window.localStorage.setItem("authToken", token);
            // On prévient Axios qu'on a maintenant un header par défaut sur toutes nos futures requetes HTTP
            setAxiosToken(token);
            const data = jwtDecode(token);
            localStorage.setItem("userInfo", JSON.stringify(data));
            dispatch({ type: USER_SIGNIN_SUCCESS, payload: data});
          });
    }
};

export const signin = (email, password) => async(dispatch) => {
   
    dispatch({ type: USER_SIGNIN_REQUEST, payload: {email, password } });
    try {
       // const data = await authAPI.authenticate(email, password);
        const data = await axios
          .post(LOGIN_API, {email, password})
          .then(response => response.data.token)
          .then(token => {
            // Je stocke le token dans mon localStorage
            window.localStorage.setItem("authToken", token);
            // On prévient Axios qu'on a maintenant un header par défaut sur toutes nos futures requetes HTTP
            setAxiosToken(token);
            const data = jwtDecode(token);
            localStorage.setItem("userInfo", JSON.stringify(data));
            dispatch({ type: USER_SIGNIN_SUCCESS, payload: data});
          });
        
        //console.log(data)
        //const data1 = jwtDecode(data);
        //console.log(data1)
        //localStorage.setItem("authToken", JSON.stringify(data));
        //localStorage.setItem("userInfo", JSON.stringify(data1));
        
    } catch(error) {
        dispatch({
            type: USER_SIGNIN_FAIL,
            payload: 
                 
            error.response && error.response.data.message 
            ? error.response.data.message
            : error.message,
        });
    } 
};

export const signout = () => (dispatch) => {
    localStorage.removeItem('userInfo');
    localStorage.removeItem('authToken');
    localStorage.removeItem('cartItems');
    localStorage.removeItem('shippingAddress');
    dispatch({ type: USER_SIGNOUT });
};

export const detailsUser = (userId) => async (dispatch, getState) => {
    dispatch({ type: USER_DETAILS_REQUEST, payload: userId})
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        const { data } =  await axios.get(USERS_API + "/" + `${userId}`, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });
        dispatch({ type: USER_DETAILS_SUCCESS, payload: data});
    }  catch(error) {
        const message = error.response && error.response.data.message 
            ? error.response.data.message
            : error.message;
        dispatch({
            type: USER_DETAILS_FAIL,
            payload: message
        });             
        
    }
};

export const updateUserProfile = (user) => async (dispatch, getState) => {
    dispatch({ type: USER_UPDATE_PROFILE_REQUEST, payload: user});
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        const { data } =  await axios.put(USERS_API + "/" + /*"id"*/`${userInfo.id}`, user, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });
        console.log(data)
        dispatch({ type: USER_UPDATE_PROFILE_SUCCESS, payload: data});
        //dispatch({type: USER_SIGNIN_SUCCESS, payload: data });
        //localStorage.setItem("userInfo", JSON.stringify(data));
    }  catch(error) {
        const message = error.response && error.response.data.message 
            ? error.response.data.message
            : error.message;
        dispatch({
            type: USER_UPDATE_PROFILE_FAIL,
            payload: message
        });         
          
        
    }
};

export const listUsers = () => async (dispatch, getState) => {
    dispatch({ type: USER_LIST_REQUEST});
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        const  data  =  await axios
            .get(USERS_API, {
                headers: {
                    Authorization: `Bearer ${token1}`,
                    //Authorization: `Bearer ${userInfo.token}`,
                },
            })
            .then(response => response.data["hydra:member"]);
        dispatch({ type: USER_LIST_SUCCESS, payload: data});
    }  catch(error) {
        const message = error.response && error.response.data.message 
            ? error.response.data.message
            : error.message;
        dispatch({
            type: USER_LIST_FAIL,
            payload: message
        });             
        
    }
};

export const deleteUser = (userId) => async (dispatch, getState) => {
    dispatch({ type: USER_DELETE_REQUEST, payload: userId})
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        const { data } =  await axios.delete(USERS_API + "/" + `${userId}`, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });
        dispatch({ type: USER_DELETE_SUCCESS, payload: data});
    }  catch(error) {
        const message = error.response && error.response.data.message 
            ? error.response.data.message
            : error.message;
        dispatch({
            type: USER_DELETE_FAIL,
            payload: message
        });             
        
    }
};

export const updateUser = (user) => async (dispatch, getState) => {
    dispatch({ type: USER_UPDATE_REQUEST, payload: user});
    const { userSignin:{userInfo}} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try {
        const { data } =  await axios.put(USERS_API + "/" + `${user.id}`, user, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });
        console.log(data)
        dispatch({ type: USER_UPDATE_SUCCESS, payload: data});
    }  catch(error) {
        const message = error.response && error.response.data.message 
            ? error.response.data.message
            : error.message;
        dispatch({
            type: USER_UPDATE_FAIL,
            payload: message
        });         
          
        
    }
};