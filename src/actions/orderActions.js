import { 
    ORDER_CREATE_FAIL, 
    ORDER_CREATE_REQUEST, 
    ORDER_CREATE_SUCCESS, 
    ORDER_DELETE_FAIL, 
    ORDER_DELETE_REQUEST, 
    ORDER_DELETE_SUCCESS, 
    ORDER_DELIVER_FAIL, 
    ORDER_DELIVER_REQUEST, 
    ORDER_DELIVER_SUCCESS, 
    ORDER_DETAILS_FAIL, 
    ORDER_DETAILS_REQUEST, 
    ORDER_DETAILS_SUCCESS, 
    ORDER_LIST_FAIL, 
    ORDER_LIST_REQUEST, 
    ORDER_LIST_SUCCESS, 
    ORDER_MINE_LIST_FAIL, 
    ORDER_MINE_LIST_REQUEST, 
    ORDER_MINE_LIST_SUCCESS, 
    ORDER_PAY_FAIL, 
    ORDER_PAY_REQUEST, 
    ORDER_PAY_SUCCESS, 
    ORDER_PRODUCTS_REQUEST
} from "../constants/orderConstants";
import { ORDERS_API, PAYMENT_RESULTS_API, DELIVS_API } from "../config.js";
import axios from "axios";
import ordersAPI from '../services/ordersAPI.js';
import { CART_EMPTY } from "../constants/cartConstants";
import productsAPI from "../services/productsAPI";
import moment from "moment";
// A voir par moiiiiiii

export const createOrder = (order) => async (dispatch, getState) => {
    dispatch({ type: ORDER_CREATE_REQUEST, payload: order });
    const {userSignin:{ userInfo }} = getState();
    const token1 = window.localStorage.getItem("authToken");
    
    const {cart:{ shippingAddress }} = getState();
    order.shippingAddress = [shippingAddress]
    //const shippingAddress1 = [window.localStorage.getItem("shippingAddress")];
    console.log([shippingAddress] )
    order.orderItems.product= order.orderItems.map((item) => (item.product = "/products/"+item.product))
    try {
        
        const { data } =  await axios.post(ORDERS_API, {
            ...order,
            //isPaid: true,
           // orderItems: ["/products/1143",
            //"/products/1144",],
            //shippingAddress: [shippingAddress],
            //shippingAddress: `/shipping_addresses/${order.shippingAddress}`,//58
            } /*order*/,{
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
            }
        );
        console.log(data)
        dispatch({ type: ORDER_CREATE_SUCCESS, payload: data });
        dispatch({ type: CART_EMPTY });
        localStorage.removeItem('cartItems');
    } catch(error) {
        dispatch({
            type: ORDER_CREATE_FAIL,
            payload: 
                error.response && error.response.data.message
                    ? error.response.data.message
                    : error.message,
        });
    }
};



export const detailsOrder = (orderId) => async (dispatch, getState) => {
    dispatch({ type: ORDER_DETAILS_REQUEST, payload: orderId });
    const {userSignin:{ userInfo }} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try{
        
        //console.log(orderId)
        const  { data } =  await axios.get(ORDERS_API + "/" + `${orderId}`, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });
       
            /*const orderItem = data.orderItems.map(async(item) => (
                console.log(item.substring(item.lastIndexOf( "/" )+1 ))
               // item = await productsAPI.find((item.substring(item.lastIndexOf( "/" )+1 )))
            //setLoading(false); 
            ))
            console.log(orderItem)*/
            /*const orderItemss =[]
                const orderItem = data.orderItems.map(async(item) => (
                    //console.log(item.substring(item.lastIndexOf( "/" )+1 ))
                    item = await productsAPI.find((item.toString().substring(item.toString().lastIndexOf( "/" )+1 ))),
                    orderItemss.push(item)
                    //item = item.substring(item.lastIndexOf( "/" )+1 ),
                    //dispatch(detailsProduct(item))
                
                ));
                 data.orderItems = orderItemss*/
               // console.log(orderItemss)
                dispatch({ type: ORDER_DETAILS_SUCCESS, payload: data  });
        //dispatch({ type: ORDER_PRODUCTS_REQUEST, payload: orderItemss.flat() });
        
    } catch(error) {
        const message = 
            error.response && error.response.data.message
                ? error.response.data.message
                : error.message;
        dispatch({ type: ORDER_DETAILS_FAIL, payload: message });
    }
};

export const payOrder = (order, paymentResult) => async (dispatch, getState) => {
    dispatch({ type: ORDER_PAY_REQUEST, payload: { order, paymentResult } });
    const {userSignin:{ userInfo }} = getState();
    const token1 = window.localStorage.getItem("authToken");
     // Gestion du format de date
     order.paymentResult = paymentResult
     const formatDate = str => moment(str).format("YYYY/MM/DD");
    console.log('order.id'+order.id)
   var data1 = {}
    try {
         const  dataPR  = await axios.post(PAYMENT_RESULTS_API , {
            //...paymentResult,
             /* id: paymentResult.id ,*/
              status: paymentResult.status ,
              emailAddress: paymentResult.payer.email_address ,
              updateTime: paymentResult.update_time ,
          }, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            } 
        });
        data1 = dataPR
        console.log(dataPR.data.id)
            
        
       
        
        
    } catch(error) {
        const message = 
            error.response && error.response.data.message
                ? error.response.data.message
                : error.message;
        dispatch({ type: ORDER_PAY_FAIL, payload: message });
    } finally {
        const  data  = await axios.put(ORDERS_API + "/" + `${order.id}`+"/pay", {
            isPaid: true,
            paidAt: formatDate(Date.now()),
            //paymentResult: paymentResult,
        // shippingAddress: `/shipping_addresses/${order.shippingAddress}`,
            paymentResult: `/payment_results/${data1.data.id}`,
        }, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            } 
        });
        console.log('payementResult'+ order.paymentResult)
        dispatch({ type: ORDER_PAY_SUCCESS, payload: data });
    }
};

export const listOrderMine = () => async (dispatch, getState) => {
    dispatch({ type: ORDER_MINE_LIST_REQUEST});
    const {userSignin:{ userInfo }} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try{
        
        const  data  = await ordersAPI.mine();
        console.log(data)
        /*const { data } =  await axios.get(ORDERS_API + "/" + `mine`, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            },
        });/*/
        dispatch({ type: ORDER_MINE_LIST_SUCCESS, payload: data });
        
    } catch(error) {
        const message = 
            error.response && error.response.data.message
                ? error.response.data.message
                : error.message;
        dispatch({ type: ORDER_MINE_LIST_FAIL, payload: message });
    }
};

export const listOrders = ({user_id = ''}) => async (dispatch, getState) => {
    dispatch({ type: ORDER_LIST_REQUEST});
    try{
        const {userSignin:{ userInfo }} = getState();
        const token1 = window.localStorage.getItem("authToken");
        
        const  data  =  await axios
            .get(ORDERS_API+ "?orderItems.product.user_id.id=" + `${user_id}`, {
                headers: {
                    Authorization: `Bearer ${token1}`,
                    //Authorization: `Bearer ${userInfo.token}`,
                },
            })
            .then(response => response.data["hydra:member"]);
        console.log(data)
        dispatch({ type: ORDER_LIST_SUCCESS, payload: data });
        
    } catch(error) {
        const message = 
            error.response && error.response.data.message
                ? error.response.data.message
                : error.message;
        dispatch({ type: ORDER_LIST_FAIL, payload: message });
    }
};

export const deleteOrder = (orderId) => async (dispatch, getState) => {
    dispatch({ type: ORDER_DELETE_REQUEST, payload: orderId});
    const {userSignin:{ userInfo }} = getState();
    const token1 = window.localStorage.getItem("authToken");
    try{
        
        
        const  { data }  =  await axios
            .delete(ORDERS_API + "/" + `${orderId}`, {
                headers: {
                    Authorization: `Bearer ${token1}`,
                    //Authorization: `Bearer ${userInfo.token}`,
                },
            });
        dispatch({ type: ORDER_DELETE_SUCCESS, payload: data });
        
    } catch(error) {
        const message = 
            error.response && error.response.data.message
                ? error.response.data.message
                : error.message;
        dispatch({ type: ORDER_DELETE_FAIL, payload: message });
    }
};

export const deliverOrder = (orderId) => async (dispatch, getState) => {
    dispatch({ type: ORDER_DELIVER_REQUEST, payload: { orderId } });
    const {userSignin:{ userInfo }} = getState();
    const token1 = window.localStorage.getItem("authToken");

    const formatDate = str => moment(str).format("YYYY/MM/DD");
    try {
        
        const { data } = await axios.put(DELIVS_API + "/" + `${orderId}` + "/deliver", { 
            isDelivered: true,
            deliveredAt: formatDate(Date.now()),
        }, {
            headers: {
                Authorization: `Bearer ${token1}`,
                //Authorization: `Bearer ${userInfo.token}`,
            } 
        });

        dispatch({ type: ORDER_DELIVER_SUCCESS, payload: data });
    } catch(error) {
        const message = 
            error.response && error.response.data.message
                ? error.response.data.message
                : error.message;
        dispatch({ type: ORDER_DELIVER_FAIL, payload: message });
    }
};