import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { detailsProduct, createReview } from '../actions/productActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import Rating from '../components/Rating';
import { PRODUCT_REVIEW_CREATE_RESET } from '../constants/productConstants';
import {Button, Card, Container, Form} from "react-bootstrap";

export default function ProductScreen(props) {
    const dispatch = useDispatch();
    const productId = props.match.params.id;
    const [qty, setQty] = useState(1);
    const productDetails = useSelector((state) => state.productDetails);
    const { loading, error, product } = productDetails;

	const userSignin = useSelector((state) => state.userSignin);
	const { userInfo } = userSignin;
  
	const productReviewCreate = useSelector((state) => state.productReviewCreate);
	const {
	  loading: loadingReviewCreate,
	  error: errorReviewCreate,
	  success: successReviewCreate,
	} = productReviewCreate;
  
	const [rating, setRating] = useState(0);
	const [content, setContent] = useState('');

    useEffect(() =>{
		if (successReviewCreate) {
			window.alert('Commentaire envoyé avec succès');
			setRating('');
			setContent('');
			dispatch({ type: PRODUCT_REVIEW_CREATE_RESET });
		  }
        dispatch(detailsProduct(productId));
    }, [dispatch, productId, successReviewCreate]);
    const addToCartHandler = () =>{
        props.history.push(`/cart/${productId}?qty=${qty}`);
    }

	const submitHandler = (e) => {
		e.preventDefault();
		if (content && rating) {
		  dispatch(
			createReview(productId, { rating: parseInt(rating, 10), content, product : `/products/${productId}`, })
		  );
		} else {
		  alert('Please enter content and rating');
		}
	  };

   
    return (
    	<Container className="my-5 mx-auto">
		    { loading ? (<LoadingBox />) : error ? (<MessageBox>{error}</MessageBox>) :
			    (
				    <div>
					    <div className="mb-4">
						    <a href="/" className="text-dark my-3">Retour à la liste des produit</a>
					    </div>
					
					    <Card className="py-0 mb-2">
						    <Card.Body className="p-0">
							    <div className="row px-3">
								    <div className="col-lg-6 py-3">
									    <div
										    style={{
											    backgroundImage: `url(${ product.image })`,
											    backgroundRepeat: 'no-repeat',
											    backgroundPosition: 'center center',
											    backgroundSize: 'cover',
											    height: '100%',
											    width: '100%',
											    padding: '100px'
									        }}
									    />
								    </div>
								    <div className="col-lg-6 mx-auto px-4 py-3">
									    <h1 className="h1 mb-2">{ product.name }</h1>
									    <Rating rating={product.rating} numReviews={product.numReviews} />
									    <h3 className="h3 my-3 pt-1">{ product.price } €</h3>
									    <p className="my-3">{ product.description }</p>
									    { product.user_id.firstName ?
										    <p>
											    Vendu par :
											    <a href={`/seller/${product.user_id.id}`} className="text-dark text-decoration-none ml-2">
												    {product.user_id.firstName ? product.user_id.companyName : ''}
											    </a>
											    <Rating
												    rating={product.user_id.rating}
												    numReviews={product.user_id.numReviews}
											    />
										    </p>
										    :
										    ""
									    }
									    <p>
										    {
											    product.countInStock >0 ? (<span className="text-success"> En stock </span>)
												    : (<span className="text-danger">En rupture de stock</span>)
										    }
									    </p>
									
									    {
										    product.countInStock > 0 && (
											    <>
												    <p>Quantité : </p>
												
												    <select value={qty} onChange={e => setQty(e.target.value)}
												            style={{ width: "100%", height: 30, padding: 5, border: '1px solid #b3b3b3', marginBottom: 15 }}
												    >
													    {
														    [...Array(product.countInStock).keys()].map(
															    (x) => (
																    <option key={x +1} value={x + 1}>
																	    {x + 1}
																    </option>
															    )
														    )
													    }
												    </select>
												
												    <button onClick={addToCartHandler} className="btn btn-success w-100 mb-3">Ajouter au panier</button>
											    </>
										    )
									    }
								    </div>
							    </div>
						    </Card.Body>
					    </Card>
					    
						<div className="my-5">
							<h3 className="mb-4">Commentaires</h3>
							
							{ product.comments.length === 0 && (
								<MessageBox>Pas de commentaire pour ce produit</MessageBox>
							)}
							
							

							{/* userInfo && product.comments.map((review) => ( review.author.id === userInfo.id)) && (
							<MessageBox>
								'You already submitted a review'
							</MessageBox>
							)*/ }
							
							<div className="col-12 px-0 mt-5">
								<h2 className="mb-3">Écrire un commentaire</h2>
								{ userInfo && product.comments.map((review) => ( review.author.id === userInfo.id)) ? (
									<Form className="form" onSubmit={submitHandler}>
										
										<Form.Group className="mb-3" controlId="formBasicEmail">
											<Form.Label>Note</Form.Label>
											<select
												id="rating"
												value={rating}
												onChange={(e) => setRating(e.target.value)}
												className="form-control"
											>
												<option value="">Votre note</option>
												<option value="1">1 - Très  peu satisfait</option>
												<option value="2">2 - Pas satisfait</option>
												<option value="3">3 - Bien</option>
												<option value="4">4 - Satisfait</option>
												<option value="5">5 - Très satifait</option>
											</select>
										</Form.Group>
										
										<Form.Group className="mb-3" controlId="formBasicEmail">
											<Form.Label>Votre commentaire</Form.Label>
											<Form.Control
												id="content"
												as="textarea"
												value={content}
												onChange={(e) => setContent(e.target.value)}
											/>
										</Form.Group>
										
										<Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">Envoyer</Button>
										
										<div>
											{ loadingReviewCreate && <LoadingBox /> }
											{ errorReviewCreate && (
												<MessageBox variant="danger">{ errorReviewCreate }</MessageBox>
											)}
										</div>
									</Form>
								) : (
									<MessageBox>
										<a href="/signin" className="text-info text-decoration-none">Connectez-vous pour ajouter un commentaire</a>
									</MessageBox>
								)}
							</div>
							
							{product.comments.map((review) => (
								<Card key={review.id} className="mb-3">
									<Card.Body>
										<Card.Title>
										<strong> {review.author.firstName}</strong> <hr></hr><Rating rating={review.rating} caption=" " /> 
										</Card.Title>
										<Card.Text>
											<p>{review.content}</p>
										</Card.Text>
									</Card.Body>
									<Card.Footer>
										<small className="text-muted">{  review.createdAt.substring(0, 10) }</small>
									</Card.Footer>
								</Card>
							))}
								
						</div>
				    </div>
			    )}
	    </Container>
    )
}