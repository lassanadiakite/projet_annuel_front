import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { savePaymentMethod } from '../actions/cartActions';
import CheckoutSteps from '../components/CheckoutSteps';
import {Button, Container, Form} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

export default function PaymentMethodScreen(props) {


    const cart = useSelector(state => state.cart);
    const { shippingAddress } = cart;
    if(!shippingAddress ) {
        props.history.push('/shipping');
    }

    const [paymentMethod, setPaymentMethod] = useState('Paypal');
    const dispatch = useDispatch();
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(savePaymentMethod(paymentMethod));
        props.history.push('/placeorder');
    }
    return (
    	<Container className="my-5 py-1 mx-auto">
		    <CheckoutSteps step1 step2 step3 />
		    <h1 className="mt-2 mb-3 text-center">Méthode de paiement</h1>
		    <div className="mt-5 mb-3 mx-auto col-md-4">
			    <Form className="form text-center" onSubmit={submitHandler}>
				
				    <Form.Group className="mb-3" id="formGridCheckbox">
					    <Form.Check
						    type="radio"
						    id="paypal"
						    value="paypal"
						    label="PayPal"
						    name="paymentMethod"
						    required
						    checked
						    onChange={(e) => setPaymentMethod(e.target.value)}
					    />
				    </Form.Group>
				
				    <Form.Group className="mb-3" id="formGridCheckbox">
					    <Form.Check
						    type="radio"
						    id="stripe"
						    value="stripe"
						    label="Stripe"
						    name="paymentMethod"
						    required
						    onChange={(e) => setPaymentMethod(e.target.value)}
					    />
				    </Form.Group>
				
				
				    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">
					    <FontAwesomeIcon icon={faArrowRight} style={{ fontSize: 20 }} />
				    </Button>
			    </Form>
		    </div>
	    </Container>
    );
}