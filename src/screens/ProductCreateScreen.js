import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {createProduct} from '../actions/productActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import {PRODUCT_CREATE_RESET} from "../constants/productConstants";
import CategoriesAPI from '../services/categoriesAPI'
import {Button, Container, Form} from "react-bootstrap";

export default function ProductCreateScreen(props) {
	const [product, setProduct] = useState({
		price: 0,
		image: "",
		category: "",
		countInStock: "",
		brand: "",
		description: ""
		
		
	});
	
	const productCreate = useSelector(state => state.productCreate);
	const {
		loading: loadingCreate,
		error: errorCreate,
		success: successCreate,
		product: createdProduct,
	} = productCreate;
	
	
	const dispatch = useDispatch();
	
	// Récupération des clients
	const [categories, setCategories] = useState([]);
	const fetchCategories = async () => {
		try {
			const data = await CategoriesAPI.findAll();
			setCategories(data);
			
			if (!product.category) setProduct({...product, category: data[0].id});
		} catch (error) {
		
		}
	};
	
	// Récupération de la liste des categories à chaque chargement du composant
	useEffect(() => {
		fetchCategories();
	}, []);
	
	useEffect(() => {
		if (successCreate) {
			dispatch({type: PRODUCT_CREATE_RESET});
			props.history.push('/productlist/seller');
		}
		
	}, [
		dispatch,
		createdProduct,
		successCreate,
		props.history,
	]);
	
	// Gestion des changements des inputs dans le formulaire
	const handleChange = ({currentTarget}) => {
		const {name, value} = currentTarget;
		setProduct({...product, [name]: value});
	};
	
	const submitHandler = (e) => {
		e.preventDefault();
		try {
			dispatch(createProduct({
				...product, category: `/categories/${product.category}`
			}));
		} catch (error) {
		
		}
	}
	
	return (
		<Container className="mx-auto my-5 py-5">
			<h1 className="mb-5 text-center h1">Ajouter un nouveau produit</h1>
			{ loadingCreate && <LoadingBox/> }
			{ errorCreate && <MessageBox variant="danger">{errorCreate}</MessageBox> }
			<Form className="form col-md-8 mx-auto" onSubmit={submitHandler}>
				<>
					<Form.Group className="mb-3">
						<Form.Control
							name="name"
							value={product.name}
							id="name"
							type="text"
							placeholder="Nom du produit"
							onChange={handleChange}
						/>
					</Form.Group>
					
					<Form.Group className="mb-3">
						<Form.Control
							name="price"
							value={product.price}
							id="price"
							type="number"
							placeholder="Prix du produit"
							onChange={handleChange}
						/>
					</Form.Group>
					
					<Form.Group className="mb-3">
						<Form.Control
							name="image"
							value={product.image}
							id="image"
							type="text"
							placeholder="Image du produit"
							onChange={handleChange}
						/>
					</Form.Group>
					
					<Form.Group className="mb-3">
						<select
							name="category"
							value={product.category}
							id="category"
							onChange={handleChange}
							className="form-control"
						>
							{categories.map(category => (
								<option key={category.id} value={category.id}>
									{category.name}
								</option>
							))}
						</select>
					</Form.Group>
					
					<Form.Group className="mb-3">
						<Form.Control
							name="brand"
							value={product.brand}
							id="brand"
							type="text"
							placeholder="Nom de la marque"
							onChange={handleChange}
						/>
					</Form.Group>
					
					<Form.Group className="mb-3">
						<Form.Control
							name="countInStock"
							value={product.countInStock}
							id="countInStock"
							type="text"
							placeholder="Nombre de produit en stock"
							onChange={handleChange}
						/>
					</Form.Group>
					
					<Form.Group className="mb-3">
						<Form.Control
							as="textarea"
							name="description"
							value={product.description}
							id="description"
							rows="3"
							type="text"
							placeholder="Description"
							onChange={handleChange}
						/>
					</Form.Group>
					
					<Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">
						Ajouter
					</Button>
				</>
			</Form>
		</Container>
	)
}