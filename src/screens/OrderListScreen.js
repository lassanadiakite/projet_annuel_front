import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {deleteOrder, listOrders} from '../actions/orderActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import {ORDER_DELETE_RESET} from '../constants/orderConstants';
import {Container, Table} from "react-bootstrap";
import {faInfoCircle, faTimes, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import moment from "moment";


export default function OrderListScreen(props) {

	const sellerMode = props.match.path.indexOf('/seller')>=0
	
	const orderList = useSelector((state) => state.orderList);
	const {loading, error, orders} = orderList;
	const orderDelete = useSelector((state) => state.orderDelete);

	const formatDate = str => moment(str).format("DD/MM/YYYY");
	
	const {
		loading: loadingDelete,
		error: errorDelete,
		success: successDelete,
	} = orderDelete;
	
	const userSignin = useSelector((state) => state.userSignin);
    const { userInfo } = userSignin;
	const dispatch = useDispatch();
	
	useEffect(() => {
		dispatch(listOrders({user_id: sellerMode ? userInfo.id : ''}));
		dispatch({type: ORDER_DELETE_RESET});
	}, [dispatch, successDelete, sellerMode, userInfo])
	
	const deleteHandler = (order) => {
		if (window.confirm('Are you sure to delete?')) {
			dispatch(deleteOrder(order.id));
		}
	}
	
	return (
		<Container className="mx-auto my-5 py-1">
			<h1 className="h1 text-center mb-5">Commandes effectués sur le site</h1>
			<div className="mt-3 mb-5">
				{ loadingDelete && <LoadingBox /> }
				{ errorDelete && <MessageBox variant="danger">{errorDelete}</MessageBox> }
				
				{ loading ? (
					<LoadingBox />
				) : error ? (
					<MessageBox variant="danger">{error}</MessageBox>
				) : (
					<Table className="table text-center" responsive>
						<thead>
							<tr>
								<th>Client</th>
								<th>Total</th>
								<th>Paiement</th>
								<th>Statut livraison</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{
								orders.map((order) => (
									<>
										<tr key={order.id}>
											<td>{order.customer.firstName} <span className="ml-2">{order.customer.lastName}</span></td>
											<td>{order.totalPrice.toFixed(2)} €</td>
											<td>
												{
													order.isPaid ?
														order.paidAt && formatDate(order.paidAt) :
														<FontAwesomeIcon icon={faTimes} style={{ fontSize: 20, color: 'red' }} />
												}
											</td>
											<td>
												{
													order.isDelivered ? order.deliveredAt && (order.deliveredAt.substring(0, 10))
													: <FontAwesomeIcon icon={faTimes} style={{ fontSize: 20, color: 'red' }} />
												}
											</td>
											
											<td>
												<a href="#" className="text-dark text-decoration-none mr-3" onClick={() => {props.history.push(`/order/${order.id}`);}}>
													<FontAwesomeIcon icon={faInfoCircle} style={{ fontSize: 20 }} />
												</a>
												
												<a href="#" className="text-dark text-decoration-none mr-2" onClick={() => deleteHandler(order)}>
													<FontAwesomeIcon icon={faTrash} style={{ fontSize: 20, color: 'red' }} />
												</a>
											</td>
										</tr>
									</>
								))
							}
						</tbody>
					</Table>
				)}
			</div>
		</Container>
	);
}