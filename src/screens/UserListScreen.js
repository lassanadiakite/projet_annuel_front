import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteUser, listUsers } from '../actions/userActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { USER_DETAILS_RESET } from '../constants/userConstants';
import {Container, Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrash, faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";

export default function UserListScreen(props) {

    const userList = useSelector((state) => state.userList);
    const { loading, error, users } = userList;

    const userDelete = useSelector((state) => state.userDelete);
    const { success: successDelete, error: errorDelete, loading: loadingDelete } = userDelete;

    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(listUsers());
        dispatch({ type: USER_DETAILS_RESET});
    }, [dispatch, successDelete]);
    
    const deleteHandler = (user) => {
        if(window.confirm("Are you sure ?")) {
            dispatch(deleteUser(user.id));
        }
    }
    
    return (
    	<Container className="my-5 py-1 mx-auto">
		    <h1 className="mt-3 mb-5 text-center">Liste des utilisateurs</h1>
		    
		    { loadingDelete && <LoadingBox /> }
		    { errorDelete && (<MessageBox variant="danger">{errorDelete}</MessageBox>) }
		
		    { successDelete && (<MessageBox variant="success">Utilisateur supprimé avec succès</MessageBox>) }
		    
		    <div>
			
			    {
				    loading ? (<LoadingBox />)
					    :
					    error ? (<MessageBox variant="danger" >{error}</MessageBox>)
						    :
						    (
							    <Table className="table text-center" responsive>
								    <thead>
								    <tr>
									    <th>Nom prénom</th>
									    <th>Adresse email</th>
									    <th>Profil vendeur</th>
									    <th>Profil admin</th>
									    <th>Actions</th>
								    </tr>
								    </thead>
								    <tbody>
								    {
									    users.map((user) =>(
										    <tr key={user.id}>
											    <td>{user.lastName} <span className="ml-2">{user.firstName}</span></td>
											    <td>{user.email}</td>
											    <td>
												    {
												    	user && user.roles.find(element => element === "ROLE_SELLER") ?
													    <FontAwesomeIcon icon={faCheck} style={{ fontSize: 20, color: 'green' }} /> :
													    <FontAwesomeIcon icon={faTimes} style={{ fontSize: 20, color: 'red' }} />
												    }
											    </td>
											    <td>
												    {
												    	user && user.roles.find(element => element === "ROLE_ADMIN") ?
														    <FontAwesomeIcon icon={faCheck} style={{ fontSize: 20, color: 'green' }} /> :
														    <FontAwesomeIcon icon={faTimes} style={{ fontSize: 20, color: 'red' }} />
												    }
											    </td>
											    <td>
												    <a href="#" className="text-dark text-decoration-none mr-3" onClick={() => props.history.push(`/user/${user.id}/edit`)}>
													    <FontAwesomeIcon icon={faEdit} style={{ fontSize: 20 }} />
												    </a>
												
												    <a href="#" className="text-dark text-decoration-none mr-2" onClick={() => deleteHandler(user)}>
													    <FontAwesomeIcon icon={faTrash} style={{ fontSize: 20, color: 'red' }} />
												    </a>
											    </td>
										    </tr>
									    ))
								    }
								    </tbody>
							    </Table>
						    )
			    }
		    </div>
	    </Container>
    )
}