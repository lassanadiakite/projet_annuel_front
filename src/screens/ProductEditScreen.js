import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { detailsProduct, updateProduct } from '../actions/productActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { PRODUCT_UPDATE_RESET } from '../constants/productConstants';
import CategoriesAPI from '../services/categoriesAPI'
import {Button, Container, Form} from "react-bootstrap";

export default function ProductEditScreen(props) {
    const productId = props.match.params.id;
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');
    const [category, setCategory] = useState('');
    const [countInStock, setCountInStock] = useState('');
    const [brand, setBrand] = useState('');
    const [description, setDescription] = useState('');

    const productDetails = useSelector(state => state.productDetails);
    const { loading, error, product} = productDetails;


    const productUpdate = useSelector(state => state.productUpdate);
    const { 
        loading: loadingUpdate, 
        error: errorUpdate, 
        success: successUpdate
    } = productUpdate;


    const dispatch = useDispatch();
    
    // Récupération des clients
    const [categories, setCategories] = useState([]);
    const fetchCategories = async () => {
        try {
        const data = await CategoriesAPI.findAll();
        setCategories(data);
        //setLoading(false);

        if (!product.customer) setCategory( data[0].id );
        } catch (error) {
        //toast.error("Impossible de charger les categories");
        //history.replace("/invoices");
        }
    };
    // Récupération de la liste des categories à chaque chargement du composant
    useEffect(() => {
        fetchCategories();
    }, []);


    useEffect(() => {
        if (successUpdate) {
            
            props.history.push('/productlist/seller');
        }
        if (!product /*|| (product.id !== productId)*/ || successUpdate) {
            dispatch({ type: PRODUCT_UPDATE_RESET});
            dispatch(detailsProduct(productId));
        } else {
            setName(product.name);
            setPrice(product.price);
            setImage(product.image);
            setCategory(product.category);
            setCountInStock(product.countInStock);
            setBrand(product.brand);
            setDescription(product.description);
        }
        
    }, [
        product,
        dispatch,
        productId,
        successUpdate,
        props.history,
    ]);
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(updateProduct({
            id: productId,
            name, price, image, category, brand, countInStock, description
        }));
    }

    return (
    	<Container className="my-5 mx-auto py-5">
		    <h1 className="h1 text-center">Modification produit : { name } </h1>
		    <Form className="form mx-auto col-md-8 mt-5" onSubmit={submitHandler}>
			
			    { loadingUpdate && <LoadingBox /> }
			    { errorUpdate && <MessageBox variant="danger">{error}</MessageBox>}
			    {
				    loading ? (
					    <LoadingBox />
				    ) : error ? (
					    <MessageBox variant="danger">{error}</MessageBox>
				    ) : (
					    <>
						    <Form.Group className="mb-3">
							    <Form.Control
								    id="name"
								    type="text"
								    placeholder="Nom du produit"
								    value={name}
								    onChange={(e) => setName(e.target.value)}
							    />
						    </Form.Group>
						
						    <Form.Group className="mb-3">
							    <Form.Control
								    id="price"
								    type="text"
								    placeholder="Prix du produit"
								    value={price}
								    onChange={(e) => setPrice(e.target.value)}
							    />
						    </Form.Group>
						
						    <Form.Group className="mb-3">
							    <Form.Control
								    id="image"
								    type="text"
								    placeholder="Lien de l'image"
								    value={image}
								    onChange={(e) => setImage(e.target.value)}
							    />
						    </Form.Group>
						
						    <Form.Group className="mb-3">
							    <select
								    aria-label="Default select example"
								    id="category"
								    value={category}
								    onChange={(e) => setCategory(e.target.value)}
								    className="form-control"
							    >
								    {categories.map(category => (
									    <option key={category.id} value={category.id}>
										    {category.name}
									    </option>
								    ))}
							    </select>
						    </Form.Group>
						
						    <Form.Group className="mb-3">
							    <Form.Control
								    id="brand"
								    type="text"
								    placeholder="Lien du logo"
								    value={brand}
								    onChange={(e) => setBrand(e.target.value)}
							    />
						    </Form.Group>
						
						    <Form.Group className="mb-3">
							    <Form.Control
								    id="countInStock"
								    type="text"
								    placeholder="Produit en stock"
								    value={countInStock}
								    onChange={(e) => setCountInStock(e.target.value)}
							    />
						    </Form.Group>
						
						    <Form.Group className="mb-3">
							    <Form.Control
								    as="textarea"
								    id="description"
								    rows="3"
								    type="text"
								    placeholder="Décription du produit"
								    value={description}
								    onChange={(e) => setDescription(e.target.value)}
							    />
						    </Form.Group>
						
						    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">Modifier</Button>
					    </>
				    )
			    }
		    </Form>
	    </Container>
    )
  }