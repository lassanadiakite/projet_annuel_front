import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom'
import { register } from '../actions/userActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import {Button, Container, Form} from "react-bootstrap";

export default function RegisterScreen(props) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState(''); 
    const [confirmPassword, setConfirmPassword] = useState(''); 

    const redirect = props.location.search ? props.location.search.split('=')[1] : '/signin';

    const userRegister = useSelector((state) => state.userRegister);
    const { userInfo, loading, error } = userRegister;
    
    const dispatch = useDispatch();
    const submitHandler = (e) => {
        e.preventDefault();
        if(password !== confirmPassword) {
            alert('Password and confirm password are not match')
        } else {
            dispatch(register(firstName, lastName, email, password))
        }
    };

    useEffect(() => {
        if(userInfo) {
            props.history.push(redirect);
        }
    }, [props.history, redirect, userInfo]);
    
    return (
	    <Container className="mx-auto my-5 py-5">
		    <h1 className="h1 text-center">Créer un compte</h1>
		
		    <div className="col-md-6 mx-auto">
			    { loading && <LoadingBox /> }
			    { error && <MessageBox variant="danger">{error}</MessageBox> }
		    </div>
		
		    <Form className="col-md-6 mx-auto" onSubmit={submitHandler}>
			    <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Control
					    type="text"
					    id="firstName"
					    placeholder="Prénom"
					    required
					    onChange={(e) => setFirstName(e.target.value)}
				    />
			    </Form.Group>
			
			    <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Control
					    type="text"
					    id="lastName"
					    placeholder="Nom"
					    required
					    onChange={(e) => setLastName(e.target.value)}
				    />
			    </Form.Group>
			
			    <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Control
					    type="email"
					    id="email"
					    placeholder="Adresse email"
					    required
					    onChange={(e) => setEmail(e.target.value)}
				    />
			    </Form.Group>
			
			    <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Control
					    type="password"
					    id="password"
					    placeholder="Mot de passe"
					    required
					    onChange={(e) => setPassword(e.target.value)}
				    />
			    </Form.Group>
			
			    <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Control
					    type="password"
					    id="confirmPassword"
					    placeholder="Confirmation de mot de passe"
					    required
					    onChange={(e) => setConfirmPassword(e.target.value)}
				    />
			    </Form.Group>
			
			    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">Enregistrer</Button>
			
			    <div className="mt-3 text-center">
				    Vous avez déjà un compte ?
				    <a href={`/signin?redirect="${redirect}`} className="text-info text-decoration-none ml-2">
					    Se connecter
				    </a>
			    </div>
		    </Form>
	    </Container>
    )
}