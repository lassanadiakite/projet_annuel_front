import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deliverOrder, detailsOrder, payOrder } from '../actions/orderActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { PayPalButton } from 'react-paypal-button-v2';
import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
import {Card, Container} from "react-bootstrap";
import moment from "moment";
export default function OrderScreen(props) {

   
    const orderId = props.match.params.id;
    const [sdkReady, setSdkReady] = useState(false);
    const orderDetails = useSelector((state) => state.orderDetails );
    const { order, loading, error } = orderDetails;
    
    const userSignin = useSelector((state) => state.userSignin);
    const { userInfo } = userSignin;

    const orderPay = useSelector((state) => state.orderPay);
    const { loading: loadingPay,
        error: errorPay, 
        success: successPay
    } = orderPay;
    const orderDeliver = useSelector((state) => state.orderDeliver);
    const { loading: loadingDeliver,
        error: errorDeliver, 
        success: successDeliver
    } = orderDeliver;
	const formatDate = str => moment(str).format("DD/MM/YYYY");
    const dispatch = useDispatch();

    // A voir by me
    useEffect(() => {
        const addPaypalScript = async () => {
			const  data  =  await 'AZBHA0M04OZw8n_9ZrNpz2gH76MSOzVRGCL5hAj4-_UjTemtWANOjMd1hX0mAQduMq9-yX8Fd4jByo9d';
            const script = document.createElement('script');
            script.type="text/javascript";
            script.src=`https://www.paypal.com/sdk/js?client-id=${data}`;
			console.log(script.src=`https://www.paypal.com/sdk/js?client-id=${data}`)
            script.async = true;
            script.onload = () => {
                setSdkReady(true);
            };
            document.body.appendChild(script);
        };
        if(!order || 
			successPay ||
			successDeliver
			) {
            dispatch({ type: ORDER_PAY_RESET });
            dispatch({ type: ORDER_DELIVER_RESET });
            dispatch(detailsOrder(orderId));
        } else {
            if(!order.isPaid) {
                if(!window.paypal) {
                    addPaypalScript();
                } else {
                    setSdkReady(true)
                }
            }
        }
        
    }, [dispatch, order, orderId, sdkReady, successPay, successDeliver]);

    const successPaymentHandler = (paymentResult) => {
        dispatch(payOrder(order, paymentResult));
    };
    const deliverHandler = () => {
        dispatch(deliverOrder(order.id));
    };

    return loading  ? (
        <LoadingBox />
    ) : error  ? (
        <MessageBox variant="danger">{error}</MessageBox>
    ) : (
    	<Container className="my-5 py-1 mx-auto">
		    <h1 className="h1 text-center mb-5">Détail de ma commande {order.id}</h1>
		    <div className="row">
			    <div className="col-md-6">
				    <>
					    { order && (
						    order.orderItems.map((item) => (
							    <div className="mb-3 border-bottom pb-3" key={item.id}>
								    <div className="d-flex justify-content-around">
									    <img src={item.image} alt={item.name} style={{ height: 50, width: 50 }}/>
									    <a href={`/product/${item.id}`} className="text-dark text-decoration-none align-self-center">{item.name}</a>
									    <span className="align-self-center"> (Qté : {item.qty} ) {item.qty * item.price} €</span>
								    </div>
							    </div>
						    ))
					    )}
				    </>
			    </div>
			    <div className="col-md-6">
				    <Card body>
					    <div className="border-bottom mb-4">
						    {order.shippingAddress.map((item) => (
							    <div className="mb-3" key={item.id}>
								    <p><strong>Nom :</strong> {item.fullName}</p>
								    <p><strong>Adresse : </strong> {item.address}, {item.postalCode} {item.city}, {item.country}</p>
							    </div>
						    ))}
						
						    {order.isDelivered ? (
							    <MessageBox variant="success">Livré le {formatDate(order.deliveredAt)}</MessageBox>
							    ) : (
							    <MessageBox variant="danger">Colis non livré</MessageBox>
							    )}
					    </div>
					    
					    <div>
						    <p><strong>Method :</strong> {order.paymentMethod}</p>
						    <p><strong>Sous total :</strong> {order.itemsPrice.toFixed(2)} €</p>
						    <p><strong>TVA :</strong> {order.taxPrice.toFixed(2)} €</p>
						    <p><strong>Total :</strong> {order.totalPrice.toFixed(2)} €</p>
						    {order.isPaid ? (
							    <MessageBox variant="success">Payé le {formatDate(order.paidAt)}</MessageBox>
						    ) : (
							    <MessageBox variant="danger">Paiement non effectué</MessageBox>
						    )}
					    </div>
					    
					    <div>
						    {
							    userInfo && userInfo.id === order.customer.id && !order.isPaid && (
								    <>
									    {!sdkReady? (
										    <LoadingBox />
									    ): (
										    <>
											    {errorPay && (
												    <MessageBox variant="danger">{errorPay}</MessageBox>
											    )}
											    {loadingPay && (
												    <LoadingBox />
											    )}
											    <PayPalButton amount={order.totalPrice} onSuccess={successPaymentHandler} />
										    </>
									    )}
								    </>
							    )
						    }
						    { userInfo && userInfo.roles[0] === "ROLE_ADMIN" && order.isPaid && !order.isDelivered  && (
							    <>
								    { loadingDeliver && <LoadingBox /> }
								    { errorDeliver && <MessageBox variant="danger">{errorDeliver}</MessageBox>}
								    <button type="button" className="primary block" onClick={deliverHandler}>
									    Procéder à la livraison
								    </button>
							    </>
						    )}
					    </div>
					    
					    <div>
						    { userInfo && userInfo.roles[0] === "ROLE_ADMIN"  && (
							    <>
								    { loadingDeliver && <LoadingBox /> }
								    { errorDeliver && <MessageBox variant="danger">{errorDeliver}</MessageBox>}
								    <button type="button" className="btn btn-success" onClick={deliverHandler}>
									    Valider la commande
								    </button>
							    </>
						    )}
					    </div>
					    
				    </Card>
			    </div>
		    </div>
	    </Container>
    );
}