import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { saveShippingAddress } from "../actions/cartActions";
import CheckoutSteps from "../components/CheckoutSteps";
import {Button, Container, Form} from "react-bootstrap";
import {faArrowRight, faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


export default function ShippingAddressScreen(props) {

    const userSignin = useSelector(state => state.userSignin);
    const {userInfo} = userSignin;
    const cart = useSelector((state) => state.cart);
    const {shippingAddress} = cart;

	const [lat, setLat] = useState(shippingAddress.lat);
	const [lng, setLng] = useState(shippingAddress.lng);
	const userAddressMap = useSelector((state) => state.userAddressMap);
	const { address: addressMap } = userAddressMap;

    if(!userInfo) {
        props.history.push('/signin');
    }

    const [fullName, setFullName] = useState(shippingAddress.fullName);
    const [address, setAddress] = useState(shippingAddress.address);
    const [city, setCity] = useState(shippingAddress.city);
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode);
    const [country, setCountry] = useState(shippingAddress.country);

    const dispatch = useDispatch();

    const submitHandler = (e) => {
        e.preventDefault();
		const newLat = addressMap ? addressMap.lat : lat;
		const newLng = addressMap ? addressMap.lng : lng;
		if (addressMap) {
		  setLat(addressMap.lat);
		  setLng(addressMap.lng);
		}
		let moveOn = true;
		if (!newLat || !newLng) {
		  moveOn = window.confirm(
			'You did not set your location on map. Continue?'
		  );
		}
		if (moveOn) {
		  dispatch(
			saveShippingAddress({
			  fullName,
			  address,
			  city,
			  postalCode,
			  country,
			  lat: newLat,
			  lng: newLng,
			})
		  );
		  props.history.push('/payment');
		}
	};
	const chooseOnMap = (e) => {
		e.preventDefault();
		dispatch(
		  saveShippingAddress({
			fullName,
			address,
			city,
			postalCode,
			country,
			lat,
			lng,
		  })
		);
		props.history.push('/map');
	};

    return (
    	<Container className="my-auto my-5 py-1">
		    <CheckoutSteps step1 step2 />
		    <h1 className="mt-3 mb-2 h1 text-center">Information d'expédition</h1>
		    <div className="mt-4 col-md-8 mx-auto">
			    <Form className="form" onSubmit={submitHandler}>
				
				    <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Control
						    type="text"
						    id="fullName"
						    placeholder="Nom complet"
						    value={fullName}
						    onChange={(e) => setFullName(e.target.value)}
						    required
					    />
				    </Form.Group>
				
				    <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Control
						    type="text"
						    id="address"
						    placeholder="Adresse postale"
						    value={address}
						    onChange={(e) => setAddress(e.target.value)}
						    required
					    />
				    </Form.Group>
				
				    <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Control
						    type="text"
						    id="city"
						    placeholder="Ville"
						    value={city}
						    onChange={(e) => setCity(e.target.value)}
						    required
					    />
				    </Form.Group>
				
				    <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Control
						    type="text"
						    id="postalCode"
						    placeholder="Code postal"
						    value={postalCode}
						    onChange={(e) => setPostalCode(e.target.value)}
						    required
					    />
				    </Form.Group>
				
				    <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Control
						    type="text"
						    id="country"
						    placeholder="Pays"
						    value={country}
						    onChange={(e) => setCountry(e.target.value)}
						    required
					    />
				    </Form.Group>
				    
					<Button variant="info" type="button" onClick={chooseOnMap} placeholder="Localisation" className="w-100 py-2 mt-2 mb-4">
					    Choisir sur la carte
				    </Button>
				    
				    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">
					    <FontAwesomeIcon icon={faArrowRight} style={{ fontSize: 20 }} />
				    </Button>
				    
			    </Form>
		    </div>
	    </Container>
    );
}