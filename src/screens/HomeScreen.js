import React, {useEffect, useState} from 'react';
import LoadingBox from '../components/LoadingBox.js';
import MessageBox from '../components/MessageBox.js';
import Product from '../components/Product.js';
import Pagination from '../components/Pagination';
import {useDispatch} from 'react-redux';
import imgHome from "../images/bg-home.jpg";
import ProductsAPI from '../services/productsAPI.js';
import CategoriesAPI from '../services/categoriesAPI';
import Rating from '../components/Rating';
import { prices, ratings } from '../utils';


import {Container, Form, Nav, Navbar, NavDropdown} from "react-bootstrap";

export default function HomeScreen(props) {
	
	const dispatch = useDispatch();
	const [products, setProducts] = useState([]);
	const [search, setSearch] = useState("");
	const [minMax, setMinMax] = useState("")
	const [currentPage, setCurrentPage] = useState(1);
	const [loading, setLoading] = useState(true);
	const [loadingCategory, setLoadingCategory] = useState(true);
	const [error, setError] = useState(false);
	const [errorCategory, setErrorCategory] = useState(false);
	
	const currentPage1 = 1
	
	const fecthData = async () => {
		try {
			setLoading(true);
			const data1 = await ProductsAPI.findAll();
			
			setProducts(data1);
			setLoading(false);
		} catch (err) {
			setError(err.message);
			setLoading(false);
		}
	};

	// Récupération de la liste des categories à chaque chargement du composant
	const [category, setCategory] = useState([]);
    const fetchCategories = async () => {
        try {
        const data2 = await CategoriesAPI.findAll();
        setCategory(data2);
        setLoadingCategory(false);
        } catch (error) {
		setErrorCategory(error.message);
			setLoadingCategory(false);
        }
    };
	useEffect(() =>{
		fecthData();
		 fetchCategories();
	}, [/*dispatch*/]);

	
	const handleSearch = ({ currentTarget }) => {
		setSearch(currentTarget.value);
		setMinMax(currentTarget.value.split(",").map(Number))
		setCurrentPage(1);
    };
	  
	 
	// Gestion du changement de page
	const handlePageChange = page => {
		setCurrentPage(page)
	}
  
	const itemsPerPage = 12;
	
	// Filtrage des products en fonction de la recherche
	 const filteredProducts = products.filter(
	  c =>
		c.name.toLowerCase().includes(search.toLowerCase()) ||

		(c.category && c.category.name.toLowerCase().includes(search.toLowerCase())) ||
		(c.user_id && c.user_id.firstName.toLowerCase().includes(search.toLowerCase())) ||
		(c.user_id && c.user_id.lastName.toLowerCase().includes(search.toLowerCase())) ||
		c.brand.toLowerCase().includes(search.toLowerCase()) ||
		//console.log(c.user && c.user_id.id ) ||
		(c.price >= minMax[0] && c.price <= minMax[1]) ||
		(c.user_id && c.user_id.rating == search) , 
	
	);
	
	const  paginatedProducts= Pagination.getData(
		filteredProducts ,
		currentPage,
		itemsPerPage
	);

	console.log(filteredProducts)
	
	return (
		<>
			<div className="container-fluid py-1" style={{
				backgroundImage: `url(${ imgHome })`,
				backgroundRepeat: 'no-repeat',
				backgroundPosition: 'center center',
				backgroundSize: 'cover'
			}}>
				<div className="col-md-8 mx-auto" style={{
					marginTop: 80,
					marginBottom: 100
				}}>
					<h1 className="h1 font-weight-bold text-white text-center mb-4">ProxiCommerce</h1>
					<Form.Group className="form-group mx-auto mb-5">
						<Form.Control
							type="text"
							placeholder="Rechercher un produit..."
							value={search}
							onChange={handleSearch}/>
					</Form.Group>
				</div>
			</div>
			<Container className="my-3 py-5 mx-auto" >
			<Navbar bg="light" expand="lg">
					<Navbar.Brand href="#">Trier</Navbar.Brand>
					<Navbar.Toggle aria-controls="navbarScroll" />
					<Navbar.Collapse id="navbarScroll">
						<Nav className="ml-auto my-2 my-lg-0">
							<NavDropdown title="Catégories" id="navbarScrollingDropdown">
								{loadingCategory ? (
									<LoadingBox />
								) : errorCategory ? (
										<MessageBox variant="danger">{errorCategory}</MessageBox>
									) :
									(
										category.map((cat) => (
											<li key={cat.id} className="mb-2 text-center">
												<button
													onClick={handleSearch}
													className="btn btn-light w-100"
													value={cat.name}
													type="button"
												> {cat.name}</button>
											</li>
										))
									)}
							</NavDropdown>
							
							<NavDropdown title="Prix" id="navbarScrollingDropdown">
								{ prices.map((p) => (
									<li key={p.name} className="mb-2 text-center">
										<button
											onClick={handleSearch}
											className="btn btn-light w-100"
											value={[p.min, p.max]}
											type="button"
										>
											{p.name}
										</button>
									</li>
								))}
							</NavDropdown>
							
							<NavDropdown title="Notation" id="navbarScrollingDropdown">
								{ ratings.map((r) => (
									<li key={r.name} className="mb-2 text-center">
										<button
											onClick={handleSearch}
											className="btn btn-light w-100"
											value={r.rating}
											type="button"
										>
											<Rating caption={' et plus'} rating={r.rating} />
										</button>
									</li>
								))}
							</NavDropdown>
						</Nav>
					</Navbar.Collapse>
				</Navbar>
				</Container>
				
			<Container className="my-3 py-5 mx-auto">
				
				
				{
					loading ? ( <LoadingBox />) : error ? (<MessageBox>{ error }</MessageBox>) :
						(
							<div className="row">
								{ paginatedProducts.map((product) => (<Product key={product.id} product={product} />)) }
							</div>
						)
				}
				
				<div className="d-flex justify-content-center">
					{
						loading ? ( <LoadingBox />) : error ? (<MessageBox>{ error }</MessageBox>) :
							(
								itemsPerPage < filteredProducts.length && (
									<Pagination
										currentPage={currentPage}
										itemsPerPage={itemsPerPage}
										length={filteredProducts.length}
										onPageChanged={handlePageChange}
									/>
								)
							)
					}
				</div>
			</Container>
		</>
	)
}