import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { createOrder } from '../actions/orderActions';
import CheckoutSteps from '../components/CheckoutSteps';
import { ORDER_CREATE_RESET } from '../constants/orderConstants';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import {Card, Container} from "react-bootstrap";
import {noAuto} from "@fortawesome/fontawesome-svg-core";

export default function PlaceOrderScreen(props) {

    const cart = useSelector((state) => state.cart);
	
    if(!cart.paymentMethod) {
        props.history.push('/payment');
    }
    const orderCreate = useSelector(state => state.orderCreate);
    const {loading, success, error, order } = orderCreate
    const toPrice = (num) => Number(num.toFixed(2)); // 5.123 => "5.12" => 5.12
    cart.itemsPrice = toPrice(
        cart.cartItems.reduce((a, c) => a + c.qty * c.price, 0)
    );
    cart.shippingPrice = cart.itemsPrice > 100 ? toPrice(0) : toPrice(10);
    cart.taxPrice = toPrice(0.15 * cart.itemsPrice);
    cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;
    const dispatch = useDispatch();
    const placeOrderHandler = () => {
		console.log('orderItems'+ cart.cartItems)
		console.log('shippingAdress'+ cart.shippingAddress)
	dispatch(createOrder({ ...cart, orderItems: cart.cartItems}));
    };

    // A voir by me
    useEffect(() => {
        if(success) {
            props.history.push(`/order/${order.id}`);
            dispatch({ type: ORDER_CREATE_RESET });
        }
    }, [dispatch, order, props.history, success]);

    return (
    	<Container className="my-5 py-1 mx-auto">
		    <CheckoutSteps step1 step2 step3 step4 />
		    <h1 className="h1 mt-3 mb-5 text-center">Récapitulatif de la commande</h1>
		    <div className="row">
			    <div className="col-md-6">
				    <>
					    {
						    cart.cartItems.map((item) => (
							    <div className="mb-3 border-bottom pb-3" key={item.product}>
								    <div className="d-flex justify-content-around">
									    <img src={item.image} alt={item.name} style={{ height: 50, width: 50 }}/>
									    <a href={`/product/${item.product}`} className="text-dark text-decoration-none align-self-center">{item.name}</a>
									    <span className="align-self-center"> (Qté : {item.qty} ) {item.qty * item.price} €</span>
								    </div>
							    </div>
						    ))
					    }
				    </>
			    </div>
			    <div className="col-md-6">
				    <Card body>
					    <div className="border-bottom mb-4">
						    <div className="mb-3">
							    <p><strong>Nom :</strong> {cart.shippingAddress.fullName}</p>
							    <p><strong>Adresse : </strong> {cart.shippingAddress.address}, {cart.shippingAddress.postalCode} {cart.shippingAddress.city}, {cart.shippingAddress.country}</p>
							    <p><strong>Méthode de paiement :</strong> {cart.paymentMethod}</p>
						    </div>
					    </div>
					    
					    <div>
						    <p>TVA : <b>{cart.taxPrice.toFixed(2)}</b></p>
						    <p>Frais de port : <b>{cart.shippingPrice.toFixed(2)}</b></p>
						    <p>Montant total : <b>{cart.totalPrice.toFixed(2)} €</b></p>
					    </div>
					
					    <div className="pt-3">
						    <button type="button" onClick={placeOrderHandler} className="btn btn-info w-100" disabled={cart.cartItems.length === 0}>
							    Payer maintenant
						    </button>
					    </div>
					
					    { loading && <LoadingBox /> }
					    { error && <MessageBox variant="danger">{error}</MessageBox> }
				    </Card>
			    </div>
		    </div>
	    </Container>
    );
}