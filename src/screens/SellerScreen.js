import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {listProducts} from '../actions/productActions';
import {detailsUser} from '../actions/userActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import Product from '../components/Product';
import Rating from '../components/Rating';
import {Card, Container} from "react-bootstrap";

export default function SellerScreen(props) {
	const sellerId = props.match.params.id;
	const userDetails = useSelector((state) => state.userDetails);
	const {loading, error, user} = userDetails;
	
	const productList = useSelector((state) => state.productList);
	const {
		loading: loadingProducts,
		error: errorProducts,
		products,
	} = productList;
	
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(detailsUser(sellerId));
		dispatch(listProducts({user_id: sellerId}));
	}, [dispatch, sellerId]);
	return (
		<Container className="my-5 py-5 mx-auto">
			<div className="mb-4">
				{loading ? (
					<LoadingBox />
				) : error ? (
					
						<MessageBox>
							<div align='center' ><a href="/signin" className="text-info text-decoration-none">Connectez-vous pour contacter le vendeur</a></div>
						</MessageBox>
					
				) : ( 
					<Card className="text-center">
						<Card.Header>Information du vendeur</Card.Header>
						<Card.Body>
							<Card.Title>{user.companyName}</Card.Title>
							<Card.Text>
								{ user.description }
							</Card.Text>
						</Card.Body>
						<a href={`mailto:${user.email}`} className="text-info text-decoration-none mb-3">Contacter le vendeur</a>
						<Card.Footer className="text-muted">
							<Rating rating={user.rating} numReviews={user.numReviews} />
						</Card.Footer>
					</Card>
				)}
			</div>
			
			<div>
				{loadingProducts ? (
					<LoadingBox />
				) : errorProducts ? (
					<MessageBox variant="danger">{errorProducts}</MessageBox>
				) : (
					<>
						{products.length === 0 && <MessageBox>No Product Found</MessageBox>}
						<div className="row center">
							{ products.map((product) => (
								<Product key={product.id} product={product} />
							))}
						</div>
					</>
				)}
			</div>
		</Container>
	);
}