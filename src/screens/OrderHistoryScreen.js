import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { listOrderMine } from '../actions/orderActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import {Container, Table} from "react-bootstrap";
import {faHistory} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import moment from "moment";

export default function OrderHistoryScreen(props) { 
    const orderMineList = useSelector((state) => state.orderMineList);
    const { loading, error, orders } = orderMineList;


	const formatDate = str => moment(str).format("DD/MM/YYYY");
    const dispatch = useDispatch();
    
    useEffect(() =>{
        dispatch(listOrderMine())
    }, [dispatch]);

   const userSignin = useSelector((state) => state.userSignin);
   const { userInfo } = userSignin;
   if(!userInfo) {
	props.history.push('/signin');
   }
    return (
    	<Container className="mx-auto my-5 py-1">
		    <h1 className="h1 text-center mt-3 pb-4">Mes commandes</h1>
		    <div className="mt-3 mx-auto">
			    { loading ? (<LoadingBox />) : error ? (
				   <MessageBox>
					   <div align='center'>Vous n'avez pas de commande pour l'instant. Merci </div>
			  		</MessageBox>
			    ) : (
				    <Table className="text-center" responsive>
					    <thead>
						    <tr>
							    <th>Référence</th>
							    <th>Date</th>
							    <th>Total</th>
							    <th>Statut paiement</th>
								<th>Date du paiement</th>
							    <th>Statut livraison</th>
							    <th>Actions</th>
						    </tr>
					    </thead>
					    <tbody>
					    {
						    orders.map((order) => (
							    <>
								    { order.customer.id === userInfo.id && (
									    <tr key={order.id}>
										    <td>{order.id}</td>
										    <td>{  formatDate(order.createdAt)}</td>
										    <td>{order.totalPrice.toFixed(2)} €</td>
											<td>{order.isPaid ? "Paiement effectué" : 'Paiement non effectué'}</td>
										    <td>{order.isPaid ? order.paidAt && formatDate(order.paidAt) : 'Paiement non effectué'}</td>
										    <td>{order.isDelivered ? order.deliveredAt && (order.deliveredAt.substring(0, 10)) : 'En cours de livraison'}</td>
										    <td>
											    <a href="#" className="text-dark text-decoration-none" onClick={() => {props.history.push(`/order/${order.id}`);} }>
												    <FontAwesomeIcon icon={faHistory} style={{ fontSize: 20 }} />
											    </a>
										    </td>
									    </tr>
								    )}
							    </>
						    ))
					    }
					    </tbody>
				    </Table>
			    )}
		    </div>
	    </Container>
    );
 }