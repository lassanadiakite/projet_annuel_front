import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { detailsUser, updateUserProfile } from '../actions/userActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { USER_UPDATE_PROFILE_RESET } from '../constants/userConstants';
import {Button, Container, Form} from "react-bootstrap";

export default function ProfileScreen() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState(''); 
    const [confirmPassword, setConfirmPassword] = useState(''); 
    const [companyName, setCompanyName] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState(''); 
    const [siren, setSiren] = useState(''); 
    const [description, setDescription] = useState(''); 

    const userSignin = useSelector((state) => state.userSignin);
    const { userInfo } = userSignin;
    const userDetails = useSelector((state) => state.userDetails);
    const { loading, error, user } = userDetails;
    const userUpdateProfile = useSelector((state) => state.userUpdateProfile);
    const { success: successUpdate, error: errorUpdate, loading: loadingUpdate } = userUpdateProfile;
    const dispatch = useDispatch();
    useEffect(() => {
        if (!user) {
            dispatch({ type: USER_UPDATE_PROFILE_RESET});
            dispatch(detailsUser(userInfo.id));
        } else {
            setFirstName(user.firstName);
            setLastName(user.lastName);
            setEmail(user.email);
            if (user.roles.find(element => element === "ROLE_SELLER")) {
                setCompanyName(user.companyName);
                setAddress(user.address);
                setPhone(user.phone);
                setSiren(user.siren);
                setDescription(user.description);
            }
        }
        
    }, [dispatch, userInfo.id, user]);
    const submitHandler = (e) => {
        e.preventDefault();
        if(password !== confirmPassword){
            alert('Password and confirm Password are not matched');
        } else if(user.roles.find(element => element === "ROLE_SELLER")) {
            dispatch(updateUserProfile({ userId: user.id, 
                firstName, 
                lastName, 
                email, 
                password,
                companyName,
                address,
                phone,
                siren,
                description,
            }));
        } else {
            dispatch(updateUserProfile({ userId: user.id, 
                firstName, 
                lastName, 
                email, 
                password,
            }));
        }
    };
    return (
    	<Container className="my-5 py-1 mx-auto">
		    <h1 className="h1 text-center py-3">Paramètres profil</h1>
		    <div className="mt-3 col-md-8 mx-auto">
			    <Form onSubmit={submitHandler}>
				    {
					    loading ?( <LoadingBox />) : error ? (<MessageBox variant="danger">{error}</MessageBox>)
						    :
						    (
						    <>
							    { loadingUpdate && <LoadingBox /> }
							    { errorUpdate && (<MessageBox variant="danger">{errorUpdate}</MessageBox>) }
							    { successUpdate && (<MessageBox variant="success">Modification enregistrée</MessageBox>) }
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="firstName"
									    type="text"
									    placeholder="Prénom"
									    value={firstName}
									    onChange={ (e) => setFirstName(e.target.value)} />
							    </Form.Group>
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="lastName"
									    type="text"
									    placeholder="Nom"
									    value={lastName}
									    onChange={ (e) => setLastName(e.target.value)} />
							    </Form.Group>
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="email"
									    type="email"
									    placeholder="Adresse email"
									    value={email}
									    onChange={ (e) => setEmail(e.target.value)} />
							    </Form.Group>
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="password"
									    type="password"
									    placeholder="Mot de passe"
									    value={password}
									    onChange={ (e) => setPassword(e.target.value)} />
							    </Form.Group>
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="confirmPassword"
									    type="password"
									    placeholder="Confirmation de mot de passe"
									    onChange={ (e) => setConfirmPassword(e.target.value)} />
							    </Form.Group>
							    
							    { user.roles.find(element => element == "ROLE_SELLER")  && (
								    <>
									    <h2 className="text-center mb-3">Entreprise</h2>
									
									    <Form.Group className="mb-3" controlId="formBasicEmail">
										    <Form.Control
											    id="companyName"
											    type="text"
											    placeholder="Dénomination sociale"
											    value={companyName} onChange={(e) => setCompanyName(e.target.value)} />
									    </Form.Group>
									
									    <Form.Group className="mb-3" controlId="formBasicEmail">
										    <Form.Control
											    id="address"
											    type="text"
											    placeholder="Adresse postale"
											    value={address} onChange={(e) => setAddress(e.target.value)} />
									    </Form.Group>
									
									    <Form.Group className="mb-3" controlId="formBasicEmail">
										    <Form.Control
											    id="phone"
											    type="text"
											    placeholder="Téléphone"
											    value={phone} onChange={(e) => setPhone(e.target.value)} />
									    </Form.Group>
									
									    <Form.Group className="mb-3" controlId="formBasicEmail">
										    <Form.Control
											    id="siren"
											    type="text"
											    placeholder="Numéro SIREN"
											    value={siren} onChange={(e) => setSiren(e.target.value)} />
									    </Form.Group>
									
									    <Form.Group className="mb-3" controlId="formBasicEmail">
										    <Form.Control
											    id="description"
											    type="text"
											    placeholder="Description"
											    value={description} onChange={(e) => setDescription(e.target.value)} />
									    </Form.Group>
								    </>
							    )}
							    <div className="mt-3 text-center">
								    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">Modifier</Button>
							    </div>
						    </>
					    )
				    }
			    </Form>
		    </div>
	    </Container>
    );
}