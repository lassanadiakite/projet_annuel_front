import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { signin } from '../actions/userActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import {Button, Container, Form} from "react-bootstrap";


export default function SigninScreen(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState(''); 

    const redirect = props.location.search
        ? props.location.search.split('=')[1]
        : '/';

    const userSignin = useSelector((state) => state.userSignin);
    const { userInfo, loading, error } = userSignin;
      

    const dispatch = useDispatch();
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(signin(email, password))
    };

    useEffect(() => {
        if(userInfo) {
            console.log(redirect)
            props.history.push(redirect);
        }
    }, [props.history, redirect, userInfo]);
    
    return (
	    <Container className="mx-auto my-5 py-1">
		    <h1 className="h1 text-center">Se connecter</h1>
		
		    <div className="col-md-6 mx-auto">
			    { loading && <LoadingBox /> }
			    { error && <MessageBox variant="danger">{error}</MessageBox> }
		    </div>
		
		    <Form className="py-3 col-md-6 mx-auto" onSubmit={submitHandler}>
			    <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Control
					    type="email"
					    id="email"
					    placeholder="Adresse email"
					    required
					    onChange={(e) => setEmail(e.target.value)}
				    />
			    </Form.Group>
			
			    <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Control
					    type="password"
					    id="password"
					    placeholder="Mot de passe"
					    required
					    onChange={(e) => setPassword(e.target.value)}
				    />
			    </Form.Group>
			
			    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">Se connecter</Button>
			
			    <div className="mt-3 text-center">
				    Vous n'avez pas de compte ?
				    <a href={`/register?redirect=${redirect}`} className="text-info text-decoration-none ml-2">
					    Créer un compte
				    </a>
			    </div>
		    </Form>
	    </Container>
    )
}