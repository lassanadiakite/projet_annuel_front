import React, {useEffect, useRef, useState} from 'react';
import LoadingBox from '../components/LoadingBox';
import {USER_ADDRESS_MAP_CONFIRM} from '../constants/userConstants';
import {
	LoadScript,
	GoogleMap,
	StandaloneSearchBox,
	Marker,
} from '@react-google-maps/api';
import axios from 'axios';
import {useDispatch} from 'react-redux';


const libs = ['places'];
const defaultLocation = {lat: 44.683333, lng: 5.716667};

// GOOGLE_API_KEY = AIzaSyCgky4Jdh5LPclC5b05-gLYM2hLttyWZSY
export default function HistoricEventScreen(props) {
	const [googleApiKey, setGoogleApiKey] = useState('');
	const [center, setCenter] = useState(defaultLocation);
	const [location, setLocation] = useState(center);
	
	const mapRef = useRef(null);
	const placeRef = useRef(null);
	const markerRef = useRef(null);
	
	useEffect(() => {
		const fetch = async () => {
			const data = await 'AIzaSyCgky4Jdh5LPclC5b05-gLYM2hLttyWZSY';
			setGoogleApiKey(data);
			getUserCurrentLocation();
		};
		fetch();
	}, []);
	
	const onLoad = (map) => {
		mapRef.current = map;
	};
	
	const onMarkerLoad = (marker) => {
		markerRef.current = marker;
	};
	const onLoadPlaces = (place) => {
		placeRef.current = place;
	};
	const onIdle = () => {
		setLocation({
			lat: mapRef.current.center.lat(),
			lng: mapRef.current.center.lng(),
		});
	};
	const onPlacesChanged = () => {
		const place = placeRef.current.getPlaces()[0].geometry.location;
		setCenter({lat: place.lat(), lng: place.lng()});
		setLocation({lat: place.lat(), lng: place.lng()});
	};
	const dispatch = useDispatch();
	const onConfirm = () => {
		const places = placeRef.current.getPlaces();
		if (places && places.length === 1) {
			dispatch({
				type: USER_ADDRESS_MAP_CONFIRM,
				payload: {
					lat: location.lat,
					lng: location.lng,
					address: places[0].formatted_address,
					name: places[0].name,
					vicinity: places[0].vicinity,
					googleAddressId: places[0].id,
				},
			});
			alert('location selected successfully.');
			props.history.push('/shipping');
		} else {
			alert('Please enter your address');
		}
	};
	
	const getUserCurrentLocation = () => {
		if (!navigator.geolocation) {
			alert('Geolocation os not supported by this browser');
		} else {
			navigator.geolocation.getCurrentPosition((position) => {
				setCenter({
					lat: position.coords.latitude,
					lng: position.coords.longitude,
				});
				setLocation({
					lat: position.coords.latitude,
					lng: position.coords.longitude,
				});
			});
		}
	};
	const mapContainerStyle = {
		width: '100vw',
		height: '79vh',
	};
//<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgky4Jdh5LPclC5b05-gLYM2hLttyWZSY&callback=initMap" type="text/javascript"></script>
	return googleApiKey ? (
		<div style={{ margin: '-1rem', height: 'calc(100% + 2rem)', overflow: 'auto'}}>
			<LoadScript libraries={libs} googleMapsApiKey={googleApiKey}>
				<GoogleMap
					id="smaple-map"
					mapContainerStyle={{ height: '80%', width: '100%', position: 'absolute', marginTop: '20px', overflow: 'auto' }}
					center={center}
					zoom={15}
					onLoad={onLoad}
					onIdle={onIdle}
				>
					<StandaloneSearchBox
						onLoad={onLoadPlaces}
						onPlacesChanged={onPlacesChanged}
					>
						<div className="map-input-box" style={{
							boxSizing: 'border-box',
							position: 'absolute',
							left: 0,
							right: 0,
							margin: '0.5rem auto',
							width: '25rem',
							height: '4rem',
							display: 'flex',
						}}>
							<input type="text" placeholder="Rechercher une adresse" style={{ borderRadius: '10px 0 0 10px', borderRight: 0 }} />
							<button type="button" className="primary" onClick={onConfirm}
							        style={{borderRadius: '0 10px 10px 0', borderLeft: 0}}>
								Confirm
							</button>
						</div>
					</StandaloneSearchBox>
					<Marker position={location} onLoad={onMarkerLoad} />
				</GoogleMap>
			</LoadScript>
		
		</div>
	) : (
		<LoadingBox/>
	);
}
