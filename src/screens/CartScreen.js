import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { addToCart, removeFromCart } from '../actions/cartActions';
import MessageBox from '../components/MessageBox';
import {Card, Container} from "react-bootstrap";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import imgHome from "../images/bg-home.jpg";

export default function CartScreen(props) {
    const productId = props.match.params.id;
    const qty = props.location.search
        ? Number(props.location.search.split('=')[1])
        : 1;
    const cart = useSelector((state) => state.cart);
    const { cartItems } = cart;
    const dispatch = useDispatch();   
    useEffect(() =>{
        if (productId) {
            dispatch(addToCart(productId, qty));
        }
    }, [dispatch, productId, qty]);
    const removeFromCartHandler = (id) => {
        dispatch(removeFromCart(id))
    };

    const checkoutHandler = () => {
        props.history.push('/signin?redirect=shipping')
    }
    return (
        <Container className="my-5 py-1 mx-auto">
	        <div className="my-3">
		        <a href="/" className="text-dark my-3">Continuer mes achats</a>
	        </div>
	        
	        <h1 className="h1 py-3">Mon panier</h1>
	        {
		        cartItems.length === 0
			        ? <MessageBox>Panier vide. <a href="/" className="text-dark text-decoration-none">Continuer mes achats</a></MessageBox> :
			        (
				        <div className="my-3">
					        {
						        cartItems.map((item) => (
							        <Card key={item.product} className="py-0 mb-2">
								        <Card.Body className="p-0">
									        <div className="row">
										        <div className="col-lg-3 mx-auto text-center py-3"
										            style={{
											            backgroundImage: `url(${ item.image })`,
											            backgroundRepeat: 'no-repeat',
											            backgroundPosition: 'center center',
											            backgroundSize: 'cover',
										            }}
										        />
										        <div className="col-lg-4 text-center mx-auto align-self-center py-3">
											        <p>
												        <a href={`/product/${item.product}`} className="text-info text-decoration-none">{ item.name }</a>
											        </p>
											        <p>{ item.price } €</p>
										        </div>
										        <div className="col-lg-2 text-center mx-auto align-self-center py-3">
											        <select value={item.qty}
											                onChange={(e) => dispatch(addToCart(item.product, Number(e.target.value)))}
											                className="w-100"
											        >
												        {
													        [...Array(item.countInStock).keys()].map(
														        (x) => (
															        <option key={x +1} value={x + 1}>
																        {x + 1}
															        </option>
														        )
													        )
												        }
											        </select>
										        </div>
										        <div className="col-lg-2 text-center mx-auto align-self-center py-3 border-left">
											        <a href="#" className="text-dark text-decoration-none" onClick={() => removeFromCartHandler(item.product)}>
												        <FontAwesomeIcon icon={faTrash} style={{ fontSize: 20 }} />
											        </a>
										        </div>
										        
									        </div>
								        </Card.Body>
							        </Card>
						        ))
					        }
				        </div>
			        )
	        }
	
	        <div className="col-12 px-0 pt-5 text-right">
		        <h4>
			        Total ({cartItems.reduce((a,c) => a + c.qty, 0)} article(s)) :
			        <span className="ml-2">{cartItems.reduce((a,c) => a + c.price * c.qty, 0)} €</span>
		        </h4>
		
		        <button type="button" onClick={checkoutHandler} className="btn btn-success ml-auto mt-3" disabled={cartItems.length === 0}>
			        Payer maintenant
		        </button>
	        </div>
        </Container>
    )
}
