import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createProduct, deleteProduct, listProducts } from "../actions/productActions";
import LoadingBox from "../components/LoadingBox";
import MessageBox from "../components/MessageBox";
import { PRODUCT_CREATE_RESET, PRODUCT_DELETE_RESET } from "../constants/productConstants";
import {Container, Table, Form} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash, faEdit} from "@fortawesome/free-solid-svg-icons";

import Pagination from '../components/Pagination';

export default function ProductListScreen(props) { 
	const sellerMode = props.match.path.indexOf('/seller')>=0
    const productList = useSelector((state) => state.productList);
    const {loading, error, products} = productList;
	const [search, setSearch] = useState("");
	const [currentPage, setCurrentPage] = useState(1);


    const productDelete = useSelector((state) => state.productDelete);
    const {
        loading:loadingDelete,
        error: errorDelete,
        success: successDelete,
    } = productDelete;

	const userSignin = useSelector((state) => state.userSignin);
    const { userInfo } = userSignin;
    const dispatch = useDispatch();
    useEffect(() =>{
     
        if (successDelete) {
            dispatch({ type: PRODUCT_DELETE_RESET});
        }
        dispatch(listProducts({user_id: sellerMode ? userInfo.id : '',}));
    }, [dispatch, props.history, sellerMode, userInfo, successDelete]);

    
    const deleteHandler = (product) => {
        if(window.confirm("Are you sure to delete?")) {
             dispatch(deleteProduct(product.id));
        }
       
    };
   


	const handleSearch = ({ currentTarget }) => {
		//dispatch(listProducts({search: currentTarget.value, currentPage: 1}));
		setSearch(currentTarget.value);
		setCurrentPage(1);
	  };
	// Gestion du changement de page
	const handlePageChange = page => {
		setCurrentPage(page)
		
		//dispatch(listProducts({currentPage: page}));
	}

	const itemsPerPage = 10;
	var filteredProducts = []
	//var paginatedProducts = []
if(!loading) {	filteredProducts = products.filter(
		 
		c =>
		  c.name.toLowerCase().includes(search.toLowerCase()) ||
		  (c.category && c.category.name.toLowerCase().includes(search.toLowerCase())) ||
		  (c.customer && c.customer.firstName.toLowerCase().includes(search.toLowerCase())) ||
		  c.brand.toLowerCase().includes(search.toLowerCase()),  /*||
		  (c.company && c.company.toLowerCase().includes(search.toLowerCase()))*/
		  
	  );
	   
	}
	const paginatedProducts= Pagination.getData(
		filteredProducts,
		currentPage,
		itemsPerPage
	   )
	   console.log(paginatedProducts)
	   const redirect = props.location.search ? props.location.search.split('=')[1] : '/productlist/create';
    
    return (
    	<Container className="my-auto py-1 my-5">
		    <h1 className="h1 text-center mt-5">Liste des produits</h1>
		    <div className="py-3 my-3 text-right">
			    <a href={`${redirect}`}  className="btn btn-info" /*onClick={createHandler}*/>Ajouter un produit</a>
		    </div>
			<Form.Group className="form-group">
				<Form.Control
					type="text"
					placeholder="Rechercher un produit..."
					value={search}
					onChange={handleSearch}/>
			</Form.Group>
		    
		    <div className="mt-5 mb-5">
			    { loadingDelete && <LoadingBox /> }
			    { errorDelete && <MessageBox variant="danger">{errorDelete}</MessageBox>}
			    
			    {
				    loading ? (<LoadingBox />) : error ? (
					    <MessageBox variant="danger">{error}</MessageBox>
				    ) :
					    (
						    <Table className="text-center" responsive>
							    <thead>
								    <tr>
									    <th>Nom du produit</th>
									    <th>Prix unitaire</th>
									    <th>Catégorie</th>
									    <th>Marque</th>
									    <th>Actions</th>
								    </tr>
							    </thead>
							    <tbody>
							    {
								    paginatedProducts.map((product) => (
									    <tr key={product.id}>
										    <td>{product.name}</td>
										    <td>{product.price} €</td>
										    <td>{product.category.name}</td>
										    <td>{product.brand}</td>
										    <td>
											    <a href="#" className="text-dark text-decoration-none mr-3" onClick={() => props.history.push(`/product/${product.id}/edit`)}>
												    <FontAwesomeIcon icon={faEdit} style={{ fontSize: 20 }} />
											    </a>
											
											    <a href="#" className="text-dark text-decoration-none" onClick={() => deleteHandler(product)}>
												    <FontAwesomeIcon icon={faTrash} style={{ fontSize: 20, color: 'red' }} />
											    </a>
										    </td>
									    </tr>
								    ))
							    }
							    </tbody>
						    </Table>
					    )
			    }
				<Container className="d-flex justify-content-center">
					{ 
						(	
							itemsPerPage < filteredProducts.length && (
								<Pagination
								currentPage={currentPage}
								itemsPerPage={itemsPerPage}
								length={filteredProducts.length}
								onPageChanged={handlePageChange}
								/>
							)
						)
					}
				</Container>
		    </div>
	    </Container>
    )
 }