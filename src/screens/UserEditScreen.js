import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { detailsUser, updateUser } from '../actions/userActions';
import { USER_UPDATE_RESET } from '../constants/userConstants';
import {Button, Container, Form} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

export default function UserEditScreen(props) {
    const userId = props.match.params.id;
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [isSeller, setIsSeller] = useState(false); 
    const [isAdmin, setIsAdmin] = useState(true);

    const userSignin = useSelector((state) => state.userSignin);
    const { userInfo } = userSignin;

    const userDetails = useSelector((state) => state.userDetails);
    const { loading, error, user } = userDetails;

    const userUpdate = useSelector((state) => state.userUpdate);
    const { success: successUpdate, error: errorUpdate, loading: loadingUpdate } = userUpdate;
    const dispatch = useDispatch();
    useEffect(() => {
        if (successUpdate) {
            dispatch({ type: USER_UPDATE_RESET});
            props.history.push('/userlist');
        }
        if (!user) {
            dispatch(detailsUser(userId));
        } else {
            const foundSeller = user.roles.find(element => element == "ROLE_SELLER");

            const foundAdmin = user.roles.find(element => element == "ROLE_ADMIN");
            if((foundSeller) &&(foundAdmin)) {
                setIsSeller(true);
                document.getElementById("isSeller").checked = true;

                setIsAdmin(true);
                 document.getElementById("isAdmin").checked = true;
            } else if(foundSeller) {
                console.log(foundSeller)
                setIsSeller(true);
                document.getElementById("isSeller").checked = true;
            } else if(foundAdmin) {
                console.log(foundAdmin)
                setIsAdmin(true);
                 document.getElementById("isAdmin").checked = true;

                
                console.log(isAdmin)
            }
            setFirstName(user.firstName);
            setLastName(user.lastName);
            setEmail(user.email);
           
        }
        
    }, [dispatch, user, userId, successUpdate, props.history]);
    const submitHandler = (e) => {
        e.preventDefault();
        if((isSeller && document.getElementById("isSeller").checked === true ) && (isAdmin && document.getElementById("isAdmin").checked === true )){
            const roles = ['ROLE_SELLER', 'ROLE_ADMIN'];
            dispatch(updateUser({ id: userId, firstName, lastName, email, roles }));
         } else if(isSeller && document.getElementById("isSeller").checked === true /*isSeller.checked === true*/) {
           const roles = ['ROLE_SELLER'];
           dispatch(updateUser({ id: userId, firstName, lastName, email, roles /*isSeller , isAdmin*/}));
            //dispatch(updateUser({ roles}));
        } else if(isAdmin && document.getElementById("isAdmin").checked === true /*isAdmin.checked == true*/) {
            const roles = ['ROLE_ADMIN'];
            dispatch(updateUser({ id: userId, firstName, lastName, email, roles /*isSeller , isAdmin*/}));
           // dispatch(updateUser({ roles}));
        } else {
            dispatch(updateUser({ id: userId, firstName, lastName, email, /*isSeller , isAdmin*/}));
        }
            
        
    }; 
    return (
    	<Container className="my-5 py-1 mx-auto">
		    <h1 className="h1 text-center mt-5 mb-5">Profil de {firstName} {lastName}</h1>
		    <div className="col-md-8 mx-auto mb-5">
			    <Form className="form" onSubmit={submitHandler}>
				    
				    {
					    loading ? (<LoadingBox />) : error ? (
						    <MessageBox variant="danger">{error}</MessageBox>
					    ) : (
						    <>
							    { loadingUpdate && <LoadingBox /> }
							    { errorUpdate && (<MessageBox variant="danger">{errorUpdate}</MessageBox>) }
							    { successUpdate && (<MessageBox variant="success">Profil modifier avec succès</MessageBox>) }
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="firstName"
									    type="text"
									    placeholder="Prénom"
									    value={firstName}
									    onChange={ (e) => setFirstName(e.target.value)}
								    />
							    </Form.Group>
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="lastName"
									    type="text"
									    placeholder="Nom"
									    value={lastName}
									    onChange={ (e) => setLastName(e.target.value)}
								    />
							    </Form.Group>
							
							    <Form.Group className="mb-3" controlId="formBasicEmail">
								    <Form.Control
									    id="email"
									    type="email"
									    placeholder="Adresse email"
									    value={email}
									    onChange={ (e) => setEmail(e.target.value)}
								    />
							    </Form.Group>
							
							    <Form.Group className="mb-3" id="formGridCheckbox">
								    <Form.Check
									    id="isSeller"
									    type="checkbox"
									    label="Profil vendeur"
									    value={isSeller}
									    onChange={ (e) => setIsSeller(e.target.checked)}
								    />
							    </Form.Group>
							
							    <Form.Group className="mb-3" id="formGridCheckbox">
								    <Form.Check
									    id="isAdmin"
									    type="checkbox"
									    label="Profil admin"
									    value={isAdmin}
									    onChange={ (e) => setIsAdmin(e.target.checked)}
								    />
							    </Form.Group>
							
							    <Button variant="info" type="submit" className="w-100 py-2 mt-2 mb-4">
								    Modifier
							    </Button>
						    </>
					    )
				    }
			    </Form>
		    </div>
	    </Container>
    )
}